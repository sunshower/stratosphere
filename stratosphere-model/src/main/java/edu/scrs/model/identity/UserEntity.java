package edu.scrs.model.identity;

import edu.scrs.model.core.TenantedEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "\"RST_USER_ENTITY\"")
public class UserEntity extends TenantedEntity<Long> {

  @Setter
  @Getter(
      onMethod_ = {
        @Id,
        @Column(name = "\"ID\""),
        @GeneratedValue(strategy = GenerationType.IDENTITY)
      })
  private Long id;
}
