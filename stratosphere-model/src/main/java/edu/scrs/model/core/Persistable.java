package edu.scrs.model.core;

import java.io.Serializable;

public interface Persistable<ID extends Serializable> {

  ID getId();

  default boolean isNew() {
    return getId() == null;
  }
}
