package edu.scrs.model.core;

import edu.scrs.model.identity.UserEntity;
import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "\"RST_TENANT\"")
public class TenantEntity implements Persistable<Long> {

  @Setter
  @Getter(
      onMethod_ = {
        @Id,
        @Column(name = "\"ID\""),
        @GeneratedValue(strategy = GenerationType.IDENTITY)
      })
  private Long id;

  @Setter
  @Getter(
      onMethod_ = {
        @Column(name = "\"TENANT_CODE\""),
      })
  private String tenantKey;

  @Setter
  @Getter(
      onMethod_ = {
        @Column(name = "\"NAME\""),
      })
  private String name;

  @Setter
  @Getter(
      onMethod_ = {
        @Column(name = "\"DESCRIPTION\""),
      })
  private String description;

  @Setter
  @Getter(
      onMethod_ = {
        @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "tenant",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            targetEntity = UserEntity.class)
      })
  private Set<UserEntity> users;

  public TenantEntity() {
    users = new HashSet<>();
  }

  public void addUser(UserEntity user) {
    user.setTenant(this);
    users.add(user);
  }

  public void removeUser(UserEntity user) {
    users.remove(user);
  }
}
