package edu.scrs.model.core;

import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public abstract class TenantedEntity<ID extends Serializable> implements Persistable<ID> {

  @Setter
  @Getter(
      onMethod_ = {
        @ManyToOne(optional = false),
        @JoinColumn(name = "\"TENANT_CODE\"", referencedColumnName = "\"TENANT_CODE\"")
      })
  private TenantEntity tenant;
}
