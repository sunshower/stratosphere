package edu.scrs.model.core;

import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public class DefaultTenantedEntity extends TenantedEntity<Long> {
  @Setter
  @Getter(onMethod_ = {})
  private Long id;
}
