package edu.scrs.model.ext;

import edu.scrs.model.core.DefaultTenantedEntity;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public class MemberEntity extends DefaultTenantedEntity {}
