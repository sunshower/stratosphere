package edu.scrs.model.identity;

import static org.junit.jupiter.api.Assertions.*;

import edu.scrs.configuration.ModelConfiguration;
import edu.scrs.model.core.TenantEntity;
import edu.scrs.model.test.EntityModelTestConfiguration;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

@Rollback
@Transactional
@SpringBootTest(classes = {ModelConfiguration.class, EntityModelTestConfiguration.class})
class UserEntityTest {

  @PersistenceContext private EntityManager entityManager;
  private UserEntity entity;
  private TenantEntity tenant;

  @BeforeEach
  void setUp() {

    tenant = new TenantEntity();
    tenant.setTenantKey("sup");
    tenant.setDescription("whatever");
    tenant.setName("whatever");

    entity = new UserEntity();
    assertTrue(entity.isNew());
    tenant.addUser(entity);
  }

  @Test
  void ensureEntityIsPersistable() {
    entityManager.persist(tenant);
    entityManager.flush();
    assertFalse(entity.isNew());
  }

  @Test
  void ensureEntityIsJoinable() {
    entityManager.persist(tenant);
    entityManager.flush();
    val e =
        entityManager
            .createQuery(
                "select e from UserEntity  e " + "join e.tenant as t where t.tenantKey = 'sup'")
            .getResultList();
    assertEquals(1, e.size());
  }
}
