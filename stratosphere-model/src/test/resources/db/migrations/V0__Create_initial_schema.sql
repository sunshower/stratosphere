create table "RST_TENANT"(
    "ID" INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    "NAME" VARCHAR(128) NOT NULL,
    "DESCRIPTION" VARCHAR(128) NOT NULL,
    "TENANT_CODE" VARCHAR(32)
);



create table "RST_USER_ENTITY" (
    "TENANT_CODE" VARCHAR(32) NOT NULL,
    "ID" INT NOT NULL PRIMARY KEY AUTO_INCREMENT
);