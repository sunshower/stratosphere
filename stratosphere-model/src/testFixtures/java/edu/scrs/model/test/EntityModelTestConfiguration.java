package edu.scrs.model.test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ContextConfiguration
@EnableTransactionManagement
public class EntityModelTestConfiguration {}
