package io.sunshower.stratosphere.api.cloud.geo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PathFeature extends Feature {

  @JsonProperty("path")
  private String path;
}
