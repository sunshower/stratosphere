package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.inventory.Enumerator;
import io.sunshower.stratosphere.api.cloud.model.network.VirtualPrivateCloud;
import io.sunshower.stratosphere.api.common.Filter;
import java.util.List;

public interface VPCEnumerator extends Enumerator<VirtualPrivateCloud> {

  List<VirtualPrivateCloud> list();

  List<VirtualPrivateCloud> list(Filter<VirtualPrivateCloud> filter);
}
