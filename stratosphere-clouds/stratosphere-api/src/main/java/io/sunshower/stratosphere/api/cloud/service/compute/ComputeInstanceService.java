package io.sunshower.stratosphere.api.cloud.service.compute;

import io.sunshower.stratosphere.api.cloud.model.compute.ComputeInstance;
import io.sunshower.stratosphere.api.cloud.service.core.AggregatedInfrastructureService;
import io.sunshower.stratosphere.api.cloud.service.core.FeatureService;
import java.util.List;

public interface ComputeInstanceService
    extends AggregatedInfrastructureService<ComputeInstance>, FeatureService {
  List<ComputeInstance> getReservedInstances();

  int getCount();
}
