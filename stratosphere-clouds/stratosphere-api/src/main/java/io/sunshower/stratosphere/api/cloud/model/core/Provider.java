package io.sunshower.stratosphere.api.cloud.model.core;

public interface Provider {

  void setKey(String key);

  String getKey();

  void setName(String provider);

  String getName();
}
