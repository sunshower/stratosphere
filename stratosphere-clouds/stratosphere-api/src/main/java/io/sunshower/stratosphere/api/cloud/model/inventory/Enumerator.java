package io.sunshower.stratosphere.api.cloud.model.inventory;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;
import java.util.List;
import java.util.concurrent.ExecutorService;

public interface Enumerator<T extends CloudElement> {

  /**
   * count the number of elements that can be enumerated by this enumerator
   *
   * @return the count
   */
  default int count() {
    return enumerate().size();
  }

  Class<T> getEnumeratedType();

  InventorySet<T> enumerate();

  InventorySet<T> enumerate(int start, int max);

  List<Enumerator<?>> getChildren();

  <U extends CloudElement> Enumerator<U> getChild(Class<U> type);

  default AsynchronousEnumerator<T> asAsync(ExecutorService service) {
    return new AsynchronousEnumerator<>(service, this);
  }

  void accept(CloudElementVisitor visitor);
}
