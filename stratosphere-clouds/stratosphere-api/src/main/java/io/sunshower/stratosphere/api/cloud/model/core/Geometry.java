package io.sunshower.stratosphere.api.cloud.model.core;

public interface Geometry {
  Double getX();

  Double getY();
}
