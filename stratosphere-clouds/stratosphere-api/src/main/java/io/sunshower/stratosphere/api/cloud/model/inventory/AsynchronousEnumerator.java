package io.sunshower.stratosphere.api.cloud.model.inventory;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import lombok.val;

public class AsynchronousEnumerator<T extends CloudElement> {

  private final ExecutorService service;
  private final Enumerator<T> enumerator;

  public AsynchronousEnumerator(ExecutorService service, Enumerator<T> tEnumerator) {
    this.service = service;
    this.enumerator = tEnumerator;
  }

  public Future<InventorySet<T>> enumerate(Consumer<InventorySet<T>> consumer) {
    return service.submit(
        () -> {
          val result = enumerator.enumerate();
          consumer.accept(result);
          return result;
        });
  }

  public Future<InventorySet<T>> enumerate() {
    return service.submit(() -> enumerator.enumerate());
  }

  public Future<InventorySet<T>> enumerate(
      Consumer<InventorySet<T>> consumer, int offset, int length) {
    return service.submit(
        () -> {
          val result = enumerator.enumerate(offset, length);
          consumer.accept(result);
          return result;
        });
  }

  public Future<InventorySet<T>> enumerate(int offset, int length) {
    return service.submit(() -> enumerator.enumerate(offset, length));
  }
}
