package io.sunshower.stratosphere.api.cloud.model.inventory;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;
import io.sunshower.stratosphere.api.cloud.model.core.RootElement;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumeratorFor {

  Class<? extends CloudElement> value();

  Class<? extends CloudElement> parent() default RootElement.class;
}
