package io.sunshower.stratosphere.api.cloud.model.core;

public interface RegionAware {
  Region getRegion();
}
