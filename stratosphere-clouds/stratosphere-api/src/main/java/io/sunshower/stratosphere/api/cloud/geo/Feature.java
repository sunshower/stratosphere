package io.sunshower.stratosphere.api.cloud.geo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Feature {

  @JsonProperty("x")
  private Double x;

  @JsonProperty("y")
  private Double y;

  @JsonProperty("name")
  private String name;

  @JsonProperty("fill")
  private Fill fill;

  @JsonProperty("stroke")
  private Stroke stroke;
}
