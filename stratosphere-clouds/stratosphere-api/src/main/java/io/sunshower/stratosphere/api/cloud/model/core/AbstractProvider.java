package io.sunshower.stratosphere.api.cloud.model.core;

public class AbstractProvider implements Provider {
  private String key;
  private String name;

  @Override
  public void setKey(String key) {
    this.key = key;
  }

  @Override
  public String getKey() {
    return key;
  }

  @Override
  public void setName(String provider) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }
}
