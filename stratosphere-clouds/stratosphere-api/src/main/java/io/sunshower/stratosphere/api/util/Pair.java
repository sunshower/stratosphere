package io.sunshower.stratosphere.api.util;

import java.util.function.BiFunction;
import java.util.function.Function;

public final class Pair<T, U> {
  public final T _1;
  public final U _2;

  private Pair(T t, U u) {
    this._1 = t;
    this._2 = u;
  }

  public T getFirst() {
    return _1;
  }

  public U getSecond() {
    return _2;
  }

  public Pair<U, T> reverse() {
    return of(_2, _1);
  }

  public <V, W> Pair<V, W> map(BiFunction<T, U, Pair<V, W>> f) {
    return f.apply(_1, _2);
  }

  public <V> Pair<V, U> mapFirst(Function<T, V> f) {
    return Pair.of(f.apply(_1), _2);
  }

  public <V> Pair<T, V> mapSecond(Function<U, V> f) {
    return Pair.of(_1, f.apply(_2));
  }

  public static <T, U> Pair<T, U> of(T t, U u) {
    return new Pair<>(t, u);
  }
}
