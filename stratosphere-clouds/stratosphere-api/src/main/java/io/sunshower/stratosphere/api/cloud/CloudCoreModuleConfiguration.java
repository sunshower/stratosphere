package io.sunshower.stratosphere.api.cloud;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;

@Configuration
@ComponentScan(
    basePackages = {
      "io.sunshower.stratosphere.api.cloud.service.core",
      "io.sunshower.stratosphere.api.cloud.service.auth"
    })
@EnableAsync
@EnableCaching
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class CloudCoreModuleConfiguration {
  public static final String EXECUTOR_SERVICE_NAME =
      "cloud:core:executors:default-executor-service";

  @Bean
  public CacheManager cacheManager() {
    return new ConcurrentMapCacheManager();
  }

  @Bean(name = EXECUTOR_SERVICE_NAME)
  public ExecutorService executorService() {
    return Executors.newCachedThreadPool();
  }

  @Bean
  public TaskExecutor taskExecutor() {
    return new ConcurrentTaskExecutor();
  }
}
