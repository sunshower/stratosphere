package io.sunshower.stratosphere.api.core;

import java.util.concurrent.atomic.AtomicReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;

@Slf4j
public class Application implements ApplicationListener<ApplicationReadyEvent> {

  private static final AtomicReference<ApplicationContext> contextReference;

  static {
    contextReference = new AtomicReference<>();
  }

  public Application() {}

  public static ApplicationContext getContext() {
    var result = contextReference.get();
    if (result != null) {
      return result;
    }
    synchronized (contextReference) {
      while ((result = contextReference.get()) == null) {
        try {
          contextReference.wait();
        } catch (InterruptedException ex) {
          log.info("Container thread interrupted");
        }
      }
    }
    return result;
  }

  @Override
  public void onApplicationEvent(ApplicationReadyEvent event) {
    synchronized (contextReference) {
      contextReference.set(event.getApplicationContext());
      contextReference.notifyAll();
    }
  }
}
