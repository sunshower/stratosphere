package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.core.Region;
import io.sunshower.stratosphere.api.cloud.model.inventory.Enumerator;
import java.util.List;

public interface RegionEnumerator extends Enumerator<Region> {

  List<Region> list();

  Region get(String id);
}
