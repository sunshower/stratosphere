package io.sunshower.stratosphere.api.cloud.service.auth;

public interface CredentialsProvider<T> {

  T getDefaultCredentials();
}
