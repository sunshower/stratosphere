package io.sunshower.stratosphere.api.cloud.model.network;

public class Subnetwork {
  private final String cidrBlock;

  public Subnetwork(String cidrBlock) {
    this.cidrBlock = cidrBlock;
  }
}
