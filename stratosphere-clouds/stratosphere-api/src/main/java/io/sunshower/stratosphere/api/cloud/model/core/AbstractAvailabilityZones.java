package io.sunshower.stratosphere.api.cloud.model.core;

public class AbstractAvailabilityZones implements AvailabilityZones {
  private Integer count;

  @Override
  public Integer getCount() {
    return count;
  }

  @Override
  public void setCount(Integer count) {
    this.count = count;
  }
}
