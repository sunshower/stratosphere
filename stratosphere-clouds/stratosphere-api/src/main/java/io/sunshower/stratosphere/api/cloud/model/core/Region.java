package io.sunshower.stratosphere.api.cloud.model.core;

public interface Region extends CloudElement {
  /**
   * the formal name of this region
   *
   * @param name
   */
  void setName(String name);

  String getName();

  Provider getProvider();

  AvailabilityZones getAvailabilityZones();

  /**
   * the informal location of this region (e.g. "N. Carolina")
   *
   * @param locationName
   */
  void setLocationName(String locationName);

  Geometry getGeometry();

  void setGeometry(Geometry geometry);
}
