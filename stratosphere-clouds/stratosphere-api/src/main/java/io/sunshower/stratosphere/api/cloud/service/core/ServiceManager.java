package io.sunshower.stratosphere.api.cloud.service.core;

import jakarta.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Service;

@Service
public class ServiceManager {
  private final ConfigurableListableBeanFactory context;

  @Inject
  public ServiceManager(final ConfigurableListableBeanFactory context) {
    this.context = context;
  }

  @SuppressWarnings("unchecked")
  public <T> List<? extends T> listServices(Class<T> type) {
    return Arrays.stream(context.getBeanNamesForType(type))
        .map(name -> (T) context.getBean(name))
        .collect(Collectors.toList());
  }
}
