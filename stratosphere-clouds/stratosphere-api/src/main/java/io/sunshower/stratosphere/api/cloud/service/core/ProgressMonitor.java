package io.sunshower.stratosphere.api.cloud.service.core;

@FunctionalInterface
public interface ProgressMonitor<T> {
  void onProgressEvent(int eventCount, int total, T value);
}
