package io.sunshower.stratosphere.api.cloud.model.core;

/** virtual element for cloud data type root */
public final class RootElement implements CloudElement {
  final Object o = new Object();

  @Override
  public Object getNativeElement() {
    return o;
  }
}
