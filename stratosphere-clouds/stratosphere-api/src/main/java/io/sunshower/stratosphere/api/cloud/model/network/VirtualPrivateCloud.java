package io.sunshower.stratosphere.api.cloud.model.network;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;

public interface VirtualPrivateCloud extends CloudElement {}
