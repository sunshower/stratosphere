package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class AbstractCloudElementService<T extends CloudElement>
    implements AggregatedInfrastructureService<T>, ApplicationContextAware {

  private ExecutorService executorService;

  public void listAsync(ProgressMonitor<Collection<T>> tracker) {
    CompletableFuture.supplyAsync(this::list, executorService)
        .thenAccept(ts -> tracker.onProgressEvent(1, 1, ts));
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.executorService = applicationContext.getBean(ExecutorService.class);
  }

  public abstract void reload();
}
