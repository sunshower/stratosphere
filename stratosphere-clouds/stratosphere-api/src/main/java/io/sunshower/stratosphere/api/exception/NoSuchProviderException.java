package io.sunshower.stratosphere.api.exception;

public class NoSuchProviderException extends SystemException {
  public NoSuchProviderException() {
    super();
  }

  public NoSuchProviderException(String message) {
    super(message);
  }

  public NoSuchProviderException(String message, Throwable cause) {
    super(message, cause);
  }

  public NoSuchProviderException(Throwable cause) {
    super(cause);
  }

  protected NoSuchProviderException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
