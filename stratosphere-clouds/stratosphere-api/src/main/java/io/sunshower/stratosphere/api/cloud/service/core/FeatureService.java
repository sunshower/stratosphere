package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.geo.Feature;
import io.sunshower.stratosphere.api.core.Application;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public interface FeatureService {

  List<Feature> loadFeatures();

  void reload();

  default ExecutorService getExecutorService() {
    return Application.getContext().getBean(ExecutorService.class);
  }

  default CompletableFuture<List<Feature>> loadFeaturesAsync() {
    return CompletableFuture.supplyAsync(
        () -> Application.getContext().getBean(getClass()).loadFeatures(), getExecutorService());
  }
}
