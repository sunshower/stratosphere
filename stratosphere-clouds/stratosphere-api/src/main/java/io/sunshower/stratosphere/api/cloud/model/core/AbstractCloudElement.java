package io.sunshower.stratosphere.api.cloud.model.core;

public class AbstractCloudElement implements CloudElement {

  private final Object nativeElement;

  protected AbstractCloudElement(Object nativeElement) {
    this.nativeElement = nativeElement;
  }

  @Override
  public Object getNativeElement() {
    return nativeElement;
  }
}
