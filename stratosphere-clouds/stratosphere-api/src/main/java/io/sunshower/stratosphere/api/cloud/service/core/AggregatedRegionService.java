package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.core.Region;
import jakarta.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.val;
import org.geotools.data.geojson.GeoJSONReader;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.util.factory.GeoTools;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.stereotype.Service;

@Service
public class AggregatedRegionService extends AbstractCloudElementService<Region>
    implements RegionService {

  private final ServiceManager serviceManager;

  private final AtomicReference<SimpleFeatureCollection> stateCache;
  private final AtomicReference<SimpleFeatureCollection> countryCache;

  @Inject
  public AggregatedRegionService(final ServiceManager serviceManager) {
    this.serviceManager = serviceManager;
    this.stateCache = new AtomicReference<>();
    this.countryCache = new AtomicReference<>();
  }

  @Override
  public List<Region> list() {
    return serviceManager.listServices(RegionEnumerator.class).stream()
        .flatMap(enumerator -> enumerator.list().stream())
        .collect(Collectors.toList());
  }

  @Override
  public Optional<Region> get(String id) {
    return serviceManager.listServices(RegionEnumerator.class).stream()
        .flatMap(
            enumerator -> {
              try {
                return Stream.of(enumerator.get(id));
              } catch (NoSuchElementException ex) {
                return Stream.empty();
              }
            })
        .findAny();
  }

  @Override
  @SneakyThrows
  public Optional<SimpleFeature> getRegionTopography(String regionId) {
    val filterFactory = CommonFactoryFinder.getFilterFactory(GeoTools.getDefaultHints());
    val filter = filterFactory.id(Set.of(filterFactory.featureId(regionId)));
    val matched = getCountries().subCollection(filter);
    if (matched.isEmpty()) {
      return Optional.empty();
    }
    try (val m = matched.features()) {
      return Optional.of(m.next());
    }
  }

  private SimpleFeatureCollection getStates() {
    return doLoad(stateCache, "META-INF/resources/assets/features/states.json");
  }

  private SimpleFeatureCollection getCountries() {
    return doLoad(countryCache, "META-INF/resources/assets/features/countries.json");
  }

  private SimpleFeatureCollection doLoad(
      AtomicReference<SimpleFeatureCollection> feature, String path) {
    return feature.updateAndGet(
        f -> {
          if (f != null) {
            return f;
          }
          try {
            val reader =
                new GeoJSONReader(
                    Thread.currentThread().getContextClassLoader().getResourceAsStream(path));
            return reader.getFeatures();
          } catch (IOException ex) {
            throw new RuntimeException(ex);
          }
        });
  }

  @Override
  public void reload() {}
}
