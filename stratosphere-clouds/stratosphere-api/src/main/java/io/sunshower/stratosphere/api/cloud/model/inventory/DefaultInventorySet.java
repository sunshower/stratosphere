package io.sunshower.stratosphere.api.cloud.model.inventory;

import java.util.Collection;
import java.util.Iterator;

public class DefaultInventorySet<T> implements InventorySet<T> {
  private final Collection<T> items;

  public DefaultInventorySet(Collection<T> list) {
    this.items = list;
  }

  @Override
  public Collection<T> getValues() {
    return items;
  }

  @Override
  public Iterator<T> iterator() {
    return items.iterator();
  }
}
