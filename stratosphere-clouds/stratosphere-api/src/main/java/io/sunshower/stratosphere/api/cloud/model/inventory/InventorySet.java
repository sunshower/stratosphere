package io.sunshower.stratosphere.api.cloud.model.inventory;

import java.util.Collection;
import java.util.Iterator;

public interface InventorySet<T> extends Iterable<T> {

  static <T> InventorySet<T> of(Collection<T> list) {
    return new DefaultInventorySet<T>(list);
  }

  Collection<? extends T> getValues();

  default int size() {
    return getValues().size();
  }

  Iterator<T> iterator();
}
