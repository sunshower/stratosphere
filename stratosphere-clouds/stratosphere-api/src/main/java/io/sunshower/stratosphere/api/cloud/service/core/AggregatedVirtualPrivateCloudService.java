package io.sunshower.stratosphere.api.cloud.service.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.sunshower.stratosphere.api.cloud.geo.Feature;
import io.sunshower.stratosphere.api.cloud.geo.PathFeature;
import io.sunshower.stratosphere.api.cloud.model.core.RegionAware;
import io.sunshower.stratosphere.api.cloud.model.network.VirtualPrivateCloud;
import jakarta.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AggregatedVirtualPrivateCloudService
    extends AbstractCloudElementService<VirtualPrivateCloud> implements VirtualPrivateCloudService {

  private final ObjectMapper objectMapper;
  private final ServiceManager serviceManager;

  @Inject
  public AggregatedVirtualPrivateCloudService(
      final ObjectMapper objectMapper, final ServiceManager serviceManager) {
    this.objectMapper = objectMapper;
    this.serviceManager = serviceManager;
  }

  @Override
  @Cacheable("clouds:aws:vpc:items")
  public List<VirtualPrivateCloud> list() {
    return serviceManager.listServices(VPCEnumerator.class).stream()
        .flatMap(enumerator -> enumerator.list().stream())
        .collect(Collectors.toList());
  }

  @Cacheable("clouds:aws:vpc:features")
  public List<Feature> loadFeatures() {
    val services = serviceManager.listServices(VPCEnumerator.class);
    val regionNames = new HashSet<String>();
    val results = new ArrayList<Feature>();
    for (val service : services) {
      for (val vpc : service.list()) {
        if (vpc instanceof RegionAware r) {
          val feature = readFeature();
          val region = r.getRegion();
          if (!regionNames.contains(region.getProvider().getKey())) {
            regionNames.add(region.getProvider().getKey());
            val geometry = region.getGeometry();
            feature.setX(geometry.getX());
            feature.setY(geometry.getY());
            results.add(feature);
          }
        }
      }
    }
    return results;
  }

  @Override
  @CacheEvict({"clouds:aws:vpc:items", "clouds:aws:vpc:features"})
  public void reload() {}

  @SneakyThrows
  private Feature readFeature() {
    return objectMapper
        .readerFor(PathFeature.class)
        .<PathFeature>readValue(
            Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("META-INF/resources/assets/features/location-pin.feature"));
  }
}
