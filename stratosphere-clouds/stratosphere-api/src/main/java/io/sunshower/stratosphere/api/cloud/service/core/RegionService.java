package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.core.Region;
import java.util.Optional;
import org.opengis.feature.simple.SimpleFeature;

public interface RegionService extends AggregatedInfrastructureService<Region> {

  Optional<Region> get(String id);

  Optional<SimpleFeature> getRegionTopography(String regionId);
}
