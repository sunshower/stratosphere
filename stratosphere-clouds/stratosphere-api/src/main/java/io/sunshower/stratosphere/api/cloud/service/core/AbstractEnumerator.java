package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;
import io.sunshower.stratosphere.api.cloud.model.core.RootElement;
import io.sunshower.stratosphere.api.cloud.model.inventory.CloudElementVisitor;
import io.sunshower.stratosphere.api.cloud.model.inventory.Enumerator;
import io.sunshower.stratosphere.api.cloud.model.inventory.EnumeratorFor;
import io.sunshower.stratosphere.api.core.Application;
import java.util.*;
import lombok.val;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;

public abstract class AbstractEnumerator<T extends CloudElement> implements Enumerator<T> {

  final Class<T> type;
  final Class<?> parentType;

  @SuppressWarnings("unchecked")
  public AbstractEnumerator() {
    val element = getClass().getAnnotation(EnumeratorFor.class);
    if (element == null) {
      throw new IllegalStateException(
          String.format(
              "Error: must annotate enumerator class with @EnumeratorFor.  Offending class: %s",
              getClass()));
    }
    this.parentType = element.parent();
    this.type = (Class<T>) element.value();
  }

  @Override
  public Class<T> getEnumeratedType() {
    return type;
  }

  @Override
  public List<Enumerator<?>> getChildren() {
    return findEnumeratorsOfType(type);
  }

  @Override
  @SuppressWarnings("unchecked")
  public <U extends CloudElement> Enumerator<U> getChild(Class<U> type) {
    return (Enumerator<U>)
        getChildren().stream()
            .filter(child -> type.isAssignableFrom(child.getClass()))
            .findAny()
            .orElseThrow(() -> new NoSuchElementException("No child enumerator of type: " + type));
  }

  @Override
  @SuppressWarnings({"unchecked"})
  public void accept(CloudElementVisitor visitor) {
    val stack = new ArrayDeque<>(findEnumeratorsOfType(RootElement.class));
    while (!stack.isEmpty()) {
      val enumerator = stack.pop();
      if (visitor.handles(enumerator.getEnumeratedType())) {
        for (val enumerated : enumerator.enumerate()) {
          visitor.visit((Enumerator) enumerator, enumerated);
        }
      }
      for (val child : enumerator.getChildren()) {
        stack.push(child);
      }
    }
  }

  protected List<Enumerator<?>> findEnumeratorsOfType(Class<?> type) {
    val ctx = (ConfigurableApplicationContext) Application.getContext();
    val beanNames = ctx.getBeanNamesForAnnotation(EnumeratorFor.class);
    val factory = (ConfigurableListableBeanFactory) ctx.getBeanFactory();
    val results = new ArrayList<Enumerator<?>>();
    for (val name : beanNames) {
      val definition = factory.getBeanDefinition(name);
      if (definition instanceof AnnotatedBeanDefinition annotatedDef) {
        val annotation =
            annotatedDef.getMetadata().getAllAnnotationAttributes(EnumeratorFor.class.getName());
        val values = annotation.get("parent");
        if (values != null) {
          for (val value : values) {
            if (Objects.equals(value, type)) {
              results.add((Enumerator<?>) ctx.getBean(name));
            }
          }
        }
      }
    }
    return results;
  }
}
