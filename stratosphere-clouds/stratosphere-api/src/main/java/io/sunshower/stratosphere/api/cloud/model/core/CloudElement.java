package io.sunshower.stratosphere.api.cloud.model.core;

public interface CloudElement {

  Object getNativeElement();
}
