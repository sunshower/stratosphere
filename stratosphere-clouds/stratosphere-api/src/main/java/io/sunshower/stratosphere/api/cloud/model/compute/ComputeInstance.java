package io.sunshower.stratosphere.api.cloud.model.compute;

import io.sunshower.stratosphere.api.cloud.model.core.AbstractCloudElement;
import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;
import io.sunshower.stratosphere.api.cloud.model.core.Region;
import io.sunshower.stratosphere.api.cloud.model.core.RegionAware;
import lombok.Getter;
import lombok.Setter;

public class ComputeInstance extends AbstractCloudElement implements CloudElement, RegionAware {

  @Getter private final Region region;

  @Getter @Setter private String cost;
  @Getter @Setter private String sku;
  @Setter @Getter private String name;
  @Setter @Getter private String instanceId;

  public ComputeInstance(final Object nativeObject, final Region region) {
    super(nativeObject);
    this.region = region;
  }
}
