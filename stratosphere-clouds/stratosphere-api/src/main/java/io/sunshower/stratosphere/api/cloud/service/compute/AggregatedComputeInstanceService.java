package io.sunshower.stratosphere.api.cloud.service.compute;

import io.sunshower.stratosphere.api.cloud.geo.Feature;
import io.sunshower.stratosphere.api.cloud.model.compute.ComputeInstance;
import io.sunshower.stratosphere.api.cloud.service.core.AbstractCloudElementService;
import io.sunshower.stratosphere.api.cloud.service.core.ServiceManager;
import jakarta.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AggregatedComputeInstanceService extends AbstractCloudElementService<ComputeInstance>
    implements ComputeInstanceService {

  private final ServiceManager serviceManager;

  @Inject
  public AggregatedComputeInstanceService(final ServiceManager serviceManager) {
    this.serviceManager = serviceManager;
  }

  @Override
  @Cacheable("clouds:aws:instances:list")
  public List<ComputeInstance> list() {
    return serviceManager.listServices(ComputeInstanceEnumerator.class).stream()
        .flatMap(enumerator -> enumerator.enumerate().getValues().stream())
        .collect(Collectors.toList());
  }

  @Override
  @Cacheable("clouds:aws:reserved-instances:list")
  public List<ComputeInstance> getReservedInstances() {
    return serviceManager.listServices(ComputeInstanceEnumerator.class).stream()
        .flatMap(enumerator -> enumerator.enumerateReservedInstances().stream())
        .collect(Collectors.toList());
  }

  @Override
  public List<Feature> loadFeatures() {
    return null;
  }

  @Override
  @CacheEvict("clouds:aws:instances:list")
  public void reload() {}

  @Override
  public int getCount() {
    return list().size();
  }
}
