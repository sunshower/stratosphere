package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;
import java.util.Collection;
import java.util.List;

public interface AggregatedInfrastructureService<T extends CloudElement> {

  default int getCount() {
    return list().size();
  }

  List<T> list();

  void listAsync(ProgressMonitor<Collection<T>> tracker);
}
