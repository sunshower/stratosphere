package io.sunshower.stratosphere.api.cloud.model.core;

public interface AvailabilityZones {

  Integer getCount();

  void setCount(Integer count);
}
