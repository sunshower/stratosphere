package io.sunshower.stratosphere.api.cloud.model.inventory;

import io.sunshower.stratosphere.api.cloud.model.core.CloudElement;

public interface CloudElementVisitor {

  <T extends CloudElement> boolean handles(Class<T> type);

  <T extends CloudElement> void visit(Enumerator<T> enumerator, T value);
}
