package io.sunshower.stratosphere.api.cloud.model.core;

public class AbstractRegion extends AbstractCloudElement implements Region {

  private String name;
  private Provider provider;
  private Geometry geometry;
  private String locationName;
  private AvailabilityZones availabilityZones;

  protected AbstractRegion(Object nativeElement, Provider provider, AvailabilityZones zones) {
    super(nativeElement);
    this.provider = provider;
    this.availabilityZones = zones;
  }

  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Provider getProvider() {
    return provider;
  }

  @Override
  public AvailabilityZones getAvailabilityZones() {
    return availabilityZones;
  }

  @Override
  public void setLocationName(String locationName) {}

  @Override
  public Geometry getGeometry() {
    return geometry;
  }

  @Override
  public void setGeometry(Geometry geometry) {
    this.geometry = geometry;
  }
}
