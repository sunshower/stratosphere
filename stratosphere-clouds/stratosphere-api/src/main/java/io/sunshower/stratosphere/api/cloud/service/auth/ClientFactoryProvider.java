package io.sunshower.stratosphere.api.cloud.service.auth;

import io.sunshower.stratosphere.api.cloud.model.core.Provider;
import io.sunshower.stratosphere.api.exception.NoSuchProviderException;
import jakarta.inject.Inject;
import java.util.Objects;
import lombok.val;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service(ClientFactoryProvider.KEY)
public class ClientFactoryProvider {
  public static final String KEY = "client-factory-provider";

  private final ApplicationContext context;

  @Inject
  public ClientFactoryProvider(ApplicationContext context) {
    this.context = context;
  }

  public ClientFactory clientFactoryFor(Provider provider) {
    val beanNames = context.getBeanNamesForType(ClientFactory.class);
    for (val beanName : beanNames) {
      try {
        val bean = context.getBean(ClientFactory.class, beanName);
        if (Objects.equals(provider.getKey(), bean.getProvider().getKey())) {
          return bean;
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    throw new NoSuchProviderException(String.format("No such provider: '%s' found", provider));
  }
}
