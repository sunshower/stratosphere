package io.sunshower.stratosphere.api.cloud.service.auth;

import io.sunshower.stratosphere.api.cloud.model.core.Provider;

public interface ClientFactory {

  Provider getProvider();

  <T> T create(Class<T> type);
}
