package io.sunshower.stratosphere.api.util;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.function.Supplier;
import org.apache.commons.lang3.StringUtils;

public class Strings {

  public static String firstNonEmpty(Supplier<String>... values) {
    return Arrays.stream(values)
        .map(Supplier::get)
        .filter(StringUtils::isNotBlank)
        .findFirst()
        .orElseThrow(() -> new NoSuchElementException("Expecting one non-blank supplier"));
  }
}
