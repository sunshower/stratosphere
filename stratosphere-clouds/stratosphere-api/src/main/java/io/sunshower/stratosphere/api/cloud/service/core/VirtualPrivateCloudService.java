package io.sunshower.stratosphere.api.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.model.network.VirtualPrivateCloud;

public interface VirtualPrivateCloudService
    extends AggregatedInfrastructureService<VirtualPrivateCloud>, FeatureService {}
