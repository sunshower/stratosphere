package io.sunshower.stratosphere.api.cloud.service.compute;

import io.sunshower.stratosphere.api.cloud.model.compute.ComputeInstance;
import io.sunshower.stratosphere.api.cloud.model.inventory.Enumerator;
import java.util.List;

public interface ComputeInstanceEnumerator extends Enumerator<ComputeInstance> {
  List<ComputeInstance> enumerateReservedInstances();
}
