package io.sunshower.stratosphere.cloud.service.core;

import io.sunshower.stratosphere.api.cloud.CloudCoreModuleConfiguration;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;

@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(
    classes = {CloudCoreModuleConfiguration.class, StratosphereAwsTestConfiguration.class})
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
public @interface StratosphereApiTest {}
