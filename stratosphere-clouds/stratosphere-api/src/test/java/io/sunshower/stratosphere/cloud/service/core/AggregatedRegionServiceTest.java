package io.sunshower.stratosphere.cloud.service.core;

import static org.junit.jupiter.api.Assertions.*;

import io.sunshower.stratosphere.api.cloud.service.core.RegionService;
import jakarta.inject.Inject;
import lombok.val;
import org.junit.jupiter.api.Test;

@StratosphereApiTest
public class AggregatedRegionServiceTest {

  @Inject private RegionService regionService;

  @Test
  void ensureContextLoads() {
    assertNotNull(regionService);
  }

  @Test
  void ensureLoadingFeaturesWorks() {
    val result = regionService.getRegionTopography("CAN");
    assertTrue(result.isPresent());
  }
}
