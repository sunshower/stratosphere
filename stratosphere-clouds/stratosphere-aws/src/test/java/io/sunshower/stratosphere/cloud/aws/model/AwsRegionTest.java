package io.sunshower.stratosphere.cloud.aws.model;

import lombok.val;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.regions.Region;

public class AwsRegionTest {
  @Test
  void ensureRegionsWorks() {
    val r = Region.regions();
    for (val rg : r) {
      System.out.format("%s\n", rg);
    }
  }
}
