package io.sunshower.stratosphere.cloud.aws.service;

import static org.junit.jupiter.api.Assertions.*;

import io.sunshower.stratosphere.api.cloud.service.core.RegionEnumerator;
import io.sunshower.stratosphere.api.cloud.service.core.VPCEnumerator;
import io.sunshower.stratosphere.cloud.aws.AwsModuleConfiguration;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AwsModuleConfiguration.class)
class AbstractEnumeratorTest {

  @Inject private RegionEnumerator regionEnumerator;

  @Inject private VPCEnumerator vpcEnumerator;

  @Test
  void ensureResolvingChildrenWorks() {
    assertEquals(regionEnumerator.getChildren().size(), 2);
  }
}
