package io.sunshower.stratosphere.cloud.aws.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.sunshower.stratosphere.api.cloud.service.core.RegionEnumerator;
import io.sunshower.stratosphere.api.cloud.service.core.ServiceManager;
import io.sunshower.stratosphere.cloud.aws.AwsModuleConfiguration;
import jakarta.inject.Inject;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AwsModuleConfiguration.class)
class AwsRegionEnumeratorTest {

  @Inject private AwsRegionEnumerator regionService;

  @Inject private ServiceManager serviceManager;

  @Test
  @SneakyThrows
  void ensureReadingNonPublicClassWorks() {
    assertEquals(17, regionService.list().size());
  }

  @Test
  void ensureServiceManagerLoads() {
    assertTrue(
        serviceManager.listServices(RegionEnumerator.class).stream()
            .anyMatch(service -> AwsRegionEnumerator.class.isAssignableFrom(service.getClass())));
  }
}
