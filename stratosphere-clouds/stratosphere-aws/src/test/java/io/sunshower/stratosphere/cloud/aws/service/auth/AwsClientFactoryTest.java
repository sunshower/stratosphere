package io.sunshower.stratosphere.cloud.aws.service.auth;

import static org.junit.jupiter.api.Assertions.*;

import io.sunshower.stratosphere.api.cloud.model.core.Provider;
import io.sunshower.stratosphere.cloud.aws.AwsModuleConfiguration;
import io.sunshower.stratosphere.cloud.aws.model.AwsProvider;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AwsModuleConfiguration.class)
class AwsClientFactoryTest {
  @Inject
  @Named(AwsProvider.KEY)
  private Provider awsProvider;

  @Test
  void ensureContextLoads() {
    assertNotNull(awsProvider);
  }
}
