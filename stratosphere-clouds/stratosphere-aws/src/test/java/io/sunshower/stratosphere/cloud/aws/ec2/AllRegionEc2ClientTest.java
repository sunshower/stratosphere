package io.sunshower.stratosphere.cloud.aws.ec2;

import static org.junit.jupiter.api.Assertions.*;

import io.sunshower.stratosphere.cloud.aws.AwsModuleConfiguration;
import jakarta.inject.Inject;
import java.util.concurrent.ExecutorService;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;

@SpringBootTest(classes = AwsModuleConfiguration.class)
class AllRegionEc2ClientTest {

  @Inject private ExecutorService executorService;
  private AllRegionEc2Client client;

  @BeforeEach
  void setUp() {
    client = new AllRegionEc2Client(executorService, DefaultCredentialsProvider.builder().build());
  }

  @Test
  @SneakyThrows
  void ensureUsingLocalCredentialsWorks() {
    val results = client.listAll();
    for (val r : results) {
      System.out.println(r.getRegion());
    }
  }
}
