package io.sunshower.stratosphere.cloud.aws.model;

import io.sunshower.stratosphere.api.cloud.model.core.AbstractRegion;
import software.amazon.awssdk.regions.Region;

public class AwsRegion extends AbstractRegion {
  public AwsRegion(Region nativeElement) {
    super(nativeElement, new AwsProvider(), new AwsAvailabilityZones());
  }
}
