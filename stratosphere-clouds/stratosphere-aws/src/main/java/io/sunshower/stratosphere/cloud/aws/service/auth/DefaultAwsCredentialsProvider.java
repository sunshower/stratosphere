package io.sunshower.stratosphere.cloud.aws.service.auth;

import io.sunshower.stratosphere.api.cloud.service.auth.CredentialsProvider;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;

public class DefaultAwsCredentialsProvider implements CredentialsProvider<AwsCredentialsProvider> {

  public static final String KEY = "stratosphere:clouds:auth:credentials-provider";

  @Override
  public AwsCredentialsProvider getDefaultCredentials() {
    return DefaultCredentialsProvider.create();
  }
}
