package io.sunshower.stratosphere.cloud.aws;

import lombok.AllArgsConstructor;
import lombok.Getter;
import software.amazon.awssdk.regions.Region;

@Getter
@AllArgsConstructor
public class RegionBoundPayload<T> {
  private final T payload;
  private final Region region;
}
