package io.sunshower.stratosphere.cloud.aws.service.auth;

import io.sunshower.stratosphere.api.cloud.CloudCoreModuleConfiguration;
import io.sunshower.stratosphere.api.cloud.model.core.Provider;
import io.sunshower.stratosphere.api.cloud.service.auth.ClientFactory;
import io.sunshower.stratosphere.api.cloud.service.auth.CredentialsProvider;
import io.sunshower.stratosphere.cloud.aws.ec2.AllRegionEc2Client;
import io.sunshower.stratosphere.cloud.aws.model.AwsProvider;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;

@Service
public class AwsClientFactory implements ClientFactory {

  private final Provider provider;
  private final ExecutorService executorService;
  private final CredentialsProvider<AwsCredentialsProvider> credentialsProvider;

  @Inject
  public AwsClientFactory(
      @Named(AwsProvider.KEY) Provider provider,
      @Named(CloudCoreModuleConfiguration.EXECUTOR_SERVICE_NAME) ExecutorService executorService,
      @Named(DefaultAwsCredentialsProvider.KEY) DefaultAwsCredentialsProvider credentialsProvider) {
    this.provider = provider;
    this.executorService = executorService;
    this.credentialsProvider = credentialsProvider;
  }

  @Override
  public Provider getProvider() {
    return provider;
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> T create(Class<T> type) {
    if (AllRegionEc2Client.class.isAssignableFrom(type)) {
      return (T)
          new AllRegionEc2Client(executorService, credentialsProvider.getDefaultCredentials());
    }
    throw new NoSuchElementException("Unable to create type: " + type);
  }
}
