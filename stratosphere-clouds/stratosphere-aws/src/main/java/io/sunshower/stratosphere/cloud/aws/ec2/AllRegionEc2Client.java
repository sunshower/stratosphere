package io.sunshower.stratosphere.cloud.aws.ec2;

import static software.amazon.awssdk.regions.Region.of;

import io.sunshower.stratosphere.api.util.Pair;
import io.sunshower.stratosphere.cloud.aws.RegionBoundPayload;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.NonNull;
import lombok.val;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.http.crt.AwsCrtAsyncHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2AsyncClient;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;
import software.amazon.awssdk.services.ec2.model.DescribeVpcsRequest;
import software.amazon.awssdk.services.ec2.model.DescribeVpcsResponse;

public class AllRegionEc2Client {

  private final Set<Region> restrictions;
  private final ExecutorService executorService;
  private final AwsCredentialsProvider credentialsProvider;

  private final Map<Region, Ec2AsyncClient> cache;

  public AllRegionEc2Client(
      @NonNull ExecutorService executorService,
      @NonNull Collection<? extends Region> restrictions,
      @NonNull AwsCredentialsProvider credentialsProvider) {
    this.executorService = executorService;
    this.restrictions = Set.copyOf(restrictions);
    this.credentialsProvider = credentialsProvider;
    this.cache = new HashMap<>();
  }

  public AllRegionEc2Client(
      @NonNull ExecutorService executorService,
      @NonNull AwsCredentialsProvider credentialsProvider) {
    this(executorService, Region.regions(), credentialsProvider);
  }

  public List<RegionBoundPayload<DescribeInstancesResponse>> listInstances(Set<Region> inList) {
    return listAll(inList, Ec2AsyncClient::describeInstances);
  }

  public List<RegionBoundPayload<DescribeVpcsResponse>> listAll(
      Consumer<DescribeVpcsRequest.Builder> filter) {
    return listAll(restrictions, client -> client.describeVpcs(filter));
  }

  public List<RegionBoundPayload<DescribeVpcsResponse>> listAll() {
    return listAll(builder -> {});
  }

  public <T> List<RegionBoundPayload<T>> listAll(
      Set<Region> regions, Function<Ec2AsyncClient, CompletableFuture<T>> fun) {
    try {
      val futures =
          regions.stream()
              .map(
                  region ->
                      Pair.of(
                          region,
                          cache.computeIfAbsent(
                              region,
                              r ->
                                  Ec2AsyncClient.builder()
                                      .credentialsProvider(credentialsProvider)
                                      .region(of(region.id()))
                                      .httpClient(
                                          AwsCrtAsyncHttpClient.builder()
                                              .connectionTimeout(Duration.ofSeconds(30))
                                              .maxConcurrency(200)
                                              .build())
                                      .build())))
              .map(
                  pair ->
                      (Callable<Pair<Region, T>>)
                          () -> pair.mapSecond(f -> fun.apply(pair._2).join()))
              .collect(Collectors.toList());
      return doInvoke(futures);
    } catch (InterruptedException ex) {
      return Collections.emptyList();
    }
  }

  private <T> List<RegionBoundPayload<T>> doInvoke(List<Callable<Pair<Region, T>>> futures)
      throws InterruptedException {
    return executorService.invokeAll(futures).stream()
        .flatMap(
            t -> {
              try {
                return Stream.of(t.get());
              } catch (InterruptedException | ExecutionException ex) {
                return Stream.empty();
              }
            })
        .map(p -> new RegionBoundPayload<>(p._2, p._1))
        .collect(Collectors.toList());
  }
}
