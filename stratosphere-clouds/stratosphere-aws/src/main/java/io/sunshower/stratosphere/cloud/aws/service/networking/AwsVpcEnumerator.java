package io.sunshower.stratosphere.cloud.aws.service.networking;

import io.sunshower.stratosphere.api.cloud.model.core.Provider;
import io.sunshower.stratosphere.api.cloud.model.core.Region;
import io.sunshower.stratosphere.api.cloud.model.inventory.EnumeratorFor;
import io.sunshower.stratosphere.api.cloud.model.inventory.InventorySet;
import io.sunshower.stratosphere.api.cloud.model.network.VirtualPrivateCloud;
import io.sunshower.stratosphere.api.cloud.service.auth.ClientFactory;
import io.sunshower.stratosphere.api.cloud.service.core.AbstractEnumerator;
import io.sunshower.stratosphere.api.cloud.service.core.RegionEnumerator;
import io.sunshower.stratosphere.api.cloud.service.core.VPCEnumerator;
import io.sunshower.stratosphere.api.common.Filter;
import io.sunshower.stratosphere.cloud.aws.RegionBoundPayload;
import io.sunshower.stratosphere.cloud.aws.ec2.AllRegionEc2Client;
import io.sunshower.stratosphere.cloud.aws.model.AwsProvider;
import io.sunshower.stratosphere.cloud.aws.model.AwsRegion;
import io.sunshower.stratosphere.cloud.aws.model.networking.AwsVpc;
import io.sunshower.stratosphere.cloud.aws.service.AwsRegionEnumerator;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.ec2.model.DescribeVpcsResponse;

@Service
@EnumeratorFor(value = VirtualPrivateCloud.class, parent = Region.class)
public class AwsVpcEnumerator extends AbstractEnumerator<VirtualPrivateCloud>
    implements VPCEnumerator {

  private final ClientFactory clientFactory;
  private final RegionEnumerator regionEnumerator;

  @Inject
  public AwsVpcEnumerator(
      @Named(AwsProvider.KEY) Provider provider,
      @Named(AwsRegionEnumerator.KEY) RegionEnumerator regionEnumerator,
      ClientFactory clientFactory) {
    this.clientFactory = clientFactory;
    this.regionEnumerator = regionEnumerator;
  }

  @Override
  public List<VirtualPrivateCloud> list() {
    return clientFactory.create(AllRegionEc2Client.class).listAll().stream()
        .flatMap(this::toVirtualPrivateCloud)
        .collect(Collectors.toList());
  }

  private Stream<AwsVpc> toVirtualPrivateCloud(RegionBoundPayload<DescribeVpcsResponse> value) {
    return value.getPayload().vpcs().stream()
        .map(
            vpc -> {
              val result = new AwsVpc();
              result.setId(vpc.vpcId());
              result.setRegion((AwsRegion) regionEnumerator.get(value.getRegion().id()));
              return result;
            });
  }

  @Override
  public List<VirtualPrivateCloud> list(Filter<VirtualPrivateCloud> filter) {
    return null;
  }

  @Override
  public InventorySet<VirtualPrivateCloud> enumerate() {
    return InventorySet.of(list());
  }

  @Override
  public InventorySet<VirtualPrivateCloud> enumerate(int start, int max) {
    return enumerate();
  }
}
