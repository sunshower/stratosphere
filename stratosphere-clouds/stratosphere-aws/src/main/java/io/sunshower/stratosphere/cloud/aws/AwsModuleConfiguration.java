package io.sunshower.stratosphere.cloud.aws;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.sunshower.stratosphere.api.cloud.CloudCoreModuleConfiguration;
import io.sunshower.stratosphere.api.cloud.model.core.Provider;
import io.sunshower.stratosphere.api.core.Application;
import io.sunshower.stratosphere.cloud.aws.model.AwsProvider;
import io.sunshower.stratosphere.cloud.aws.service.auth.DefaultAwsCredentialsProvider;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(CloudCoreModuleConfiguration.class)
@ComponentScan(basePackages = "io.sunshower.stratosphere.cloud.aws")
public class AwsModuleConfiguration {

  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper();
  }

  @Bean(name = AwsProvider.KEY)
  public Provider provider() {
    val p = new AwsProvider();
    p.setKey("aws");
    return p;
  }

  @Bean
  public Application application() {
    return new Application();
  }

  @Bean(name = DefaultAwsCredentialsProvider.KEY)
  public DefaultAwsCredentialsProvider awsCredentialsCredentialsProvider() {
    return new DefaultAwsCredentialsProvider();
  }
}
