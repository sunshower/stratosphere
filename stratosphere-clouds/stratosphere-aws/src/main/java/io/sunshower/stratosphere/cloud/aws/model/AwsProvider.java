package io.sunshower.stratosphere.cloud.aws.model;

import io.sunshower.stratosphere.api.cloud.model.core.AbstractProvider;

public class AwsProvider extends AbstractProvider {
  public static final String KEY = "stratosphere:cloud:providers:aws";
}
