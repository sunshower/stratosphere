package io.sunshower.stratosphere.cloud.aws.model.networking;

import io.sunshower.stratosphere.api.cloud.model.core.RegionAware;
import io.sunshower.stratosphere.api.cloud.model.network.Subnetwork;
import io.sunshower.stratosphere.api.cloud.model.network.VirtualPrivateCloud;
import io.sunshower.stratosphere.cloud.aws.model.AwsRegion;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AwsVpc implements VirtualPrivateCloud, RegionAware {

  private String id;
  private AwsRegion region;
  private Subnetwork subnet;

  @Override
  public Object getNativeElement() {
    return null;
  }
}
