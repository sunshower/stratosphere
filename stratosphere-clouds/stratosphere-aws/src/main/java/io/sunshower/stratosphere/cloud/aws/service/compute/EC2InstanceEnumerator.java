package io.sunshower.stratosphere.cloud.aws.service.compute;

import io.sunshower.stratosphere.api.cloud.model.compute.ComputeInstance;
import io.sunshower.stratosphere.api.cloud.model.inventory.EnumeratorFor;
import io.sunshower.stratosphere.api.cloud.model.inventory.InventorySet;
import io.sunshower.stratosphere.api.cloud.service.auth.ClientFactory;
import io.sunshower.stratosphere.api.cloud.service.compute.ComputeInstanceEnumerator;
import io.sunshower.stratosphere.api.cloud.service.core.AbstractEnumerator;
import io.sunshower.stratosphere.api.cloud.service.core.RegionService;
import io.sunshower.stratosphere.api.util.Strings;
import io.sunshower.stratosphere.cloud.aws.RegionBoundPayload;
import io.sunshower.stratosphere.cloud.aws.ec2.AllRegionEc2Client;
import jakarta.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2AsyncClient;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;
import software.amazon.awssdk.services.ec2.model.DescribeReservedInstancesResponse;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.ReservedInstances;

@Service
@EnumeratorFor(
    value = ComputeInstance.class,
    parent = io.sunshower.stratosphere.api.cloud.model.core.Region.class)
public class EC2InstanceEnumerator extends AbstractEnumerator<ComputeInstance>
    implements ComputeInstanceEnumerator {

  private final ClientFactory clientFactory;
  private final RegionService regionService;

  @Inject
  public EC2InstanceEnumerator(ClientFactory clientFactory, RegionService regionService) {
    this.regionService = regionService;
    this.clientFactory = clientFactory;
  }

  @Override
  public InventorySet<ComputeInstance> enumerate() {
    return InventorySet.of(
        clientFactory
            .create(AllRegionEc2Client.class)
            .listAll(new HashSet<>(Region.regions()), Ec2AsyncClient::describeInstances)
            .stream()
            .flatMap(
                r -> regionService.get(r.getRegion().id()).stream().flatMap(c -> this.create(c, r)))
            .collect(Collectors.toList()));
  }

  @Override
  public List<ComputeInstance> enumerateReservedInstances() {
    return clientFactory
        .create(AllRegionEc2Client.class)
        .listAll(new HashSet<>(Region.regions()), Ec2AsyncClient::describeReservedInstances)
        .stream()
        .flatMap(
            r ->
                regionService.get(r.getRegion().id()).stream()
                    .flatMap(c -> this.createReservedInstance(c, r)))
        .collect(Collectors.toList());
  }

  private Stream<ComputeInstance> createReservedInstance(
      io.sunshower.stratosphere.api.cloud.model.core.Region c,
      RegionBoundPayload<DescribeReservedInstancesResponse> r) {
    val payload = r.getPayload();
    val reservedInstances = payload.reservedInstances();
    if (reservedInstances != null) {
      return reservedInstances.stream().map(ri -> toReservedInstance(ri, c));
    }
    return Stream.empty();
  }

  private ComputeInstance toReservedInstance(
      ReservedInstances ri, io.sunshower.stratosphere.api.cloud.model.core.Region region) {
    val result = new ComputeInstance(ri, region);
    result.setInstanceId(ri.reservedInstancesId());
    result.setCost(String.format("%s %f", ri.currencyCodeAsString(), ri.fixedPrice()));
    result.setSku(ri.instanceTypeAsString());
    return result;
  }

  private Stream<ComputeInstance> create(
      io.sunshower.stratosphere.api.cloud.model.core.Region c,
      RegionBoundPayload<DescribeInstancesResponse> r) {

    val resp = r.getPayload();
    val reservations = resp.reservations();
    val result = new ArrayList<ComputeInstance>();
    for (val reservation : reservations) {
      for (val instance : reservation.instances()) {
        val computeInstance = new ComputeInstance(instance, c);
        computeInstance.setInstanceId(instance.instanceId());
        computeInstance.setName(nameOf(instance));
        result.add(computeInstance);
      }
    }
    return result.stream();
  }

  private String nameOf(Instance instance) {
    return Strings.firstNonEmpty(
        instance::publicDnsName,
        instance::privateDnsName,
        instance::publicIpAddress,
        instance::privateIpAddress,
        () -> "<>");
  }

  @Override
  public InventorySet<ComputeInstance> enumerate(int start, int max) {
    return enumerate();
  }
}
