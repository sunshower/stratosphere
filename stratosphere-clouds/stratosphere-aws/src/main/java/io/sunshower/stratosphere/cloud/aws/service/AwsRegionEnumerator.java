package io.sunshower.stratosphere.cloud.aws.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.sunshower.stratosphere.api.cloud.model.core.Region;
import io.sunshower.stratosphere.api.cloud.model.inventory.EnumeratorFor;
import io.sunshower.stratosphere.api.cloud.model.inventory.InventorySet;
import io.sunshower.stratosphere.api.cloud.service.core.AbstractEnumerator;
import io.sunshower.stratosphere.api.cloud.service.core.RegionEnumerator;
import io.sunshower.stratosphere.cloud.aws.model.AwsRegion;
import jakarta.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import lombok.val;
import org.springframework.stereotype.Component;

@Component(AwsRegionEnumerator.KEY)
@EnumeratorFor(value = Region.class)
public class AwsRegionEnumerator extends AbstractEnumerator<Region> implements RegionEnumerator {

  public static final String KEY = "clouds:services:provider:aws:region-service";

  private final ObjectMapper objectMapper;

  private static final Map<String, RegionDescriptorList.RegionDescriptor> cachedList;

  static final String LOCATION = "assets/regions/locations.json";

  static {
    cachedList = new ConcurrentHashMap<>();
  }

  @Inject
  public AwsRegionEnumerator(final ObjectMapper mapper) {
    this.objectMapper = mapper;
  }

  @Override
  public List<Region> list() {
    val dataCenterFeatures = resolveDataCenterFeatures();
    return dataCenterFeatures.entrySet().stream().map(this::bindRegion).toList();
  }

  public Region get(String key) {
    return list().stream()
        .filter(r -> Objects.equals(r.getProvider().getKey(), key))
        .findAny()
        .orElseThrow(() -> new NoSuchElementException("Error: no region: " + key));
  }

  private Region bindRegion(Map.Entry<String, RegionDescriptorList.RegionDescriptor> kv) {
    val v = kv.getValue();
    val ps = v.getProperties();
    val awsRegion = new AwsRegion(software.amazon.awssdk.regions.Region.of(kv.getKey()));
    awsRegion.setName(ps.getPlace());
    awsRegion.getProvider().setKey(ps.getRegion());
    awsRegion.getProvider().setName(ps.getProvider());
    awsRegion.getAvailabilityZones().setCount(6);
    awsRegion.setLocationName(ps.getPlace());
    awsRegion.setGeometry(v.getGeometry());
    return awsRegion;
  }

  private Map<String, RegionDescriptorList.RegionDescriptor> resolveDataCenterFeatures() {
    if (cachedList.isEmpty()) {
      try (val input =
          Thread.currentThread().getContextClassLoader().getResourceAsStream(LOCATION)) {
        val list =
            objectMapper
                .readerFor(RegionDescriptorList.class)
                .<RegionDescriptorList>readValue(input);
        for (val dataCenter : list.getDescriptors()) {
          cachedList.put(dataCenter.getProperties().getRegion(), dataCenter);
        }
      } catch (IOException ex) {
        throw new IllegalStateException("Missing resource: " + LOCATION);
      }
    }
    return cachedList;
  }

  @Override
  public int count() {
    return list().size();
  }

  @Override
  public InventorySet<Region> enumerate() {
    return InventorySet.of(list());
  }

  @Override
  public InventorySet<Region> enumerate(int start, int max) {
    return enumerate();
  }
}
