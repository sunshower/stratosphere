package io.sunshower.stratosphere.cloud.aws.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.sunshower.stratosphere.api.cloud.model.core.Geometry;
import java.util.List;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegionDescriptorList {

  @JsonProperty("data-centers")
  private List<RegionDescriptor> descriptors;

  @Getter
  @JsonIgnoreProperties(ignoreUnknown = true)
  static class RegionDescriptor {
    @JsonProperty("type")
    private String type;

    @JsonProperty RegionGeometry geometry;

    @JsonProperty private RegionDescriptorProperties properties;
  }

  @Getter
  @JsonIgnoreProperties(ignoreUnknown = true)
  static class RegionDescriptorProperties {
    @JsonProperty private String category;
    @JsonProperty private String region;
    @JsonProperty private String place;
    @JsonProperty private String provider;

    @JsonProperty("availability-zones")
    private Integer availabilityZones;
  }

  @Getter
  static class RegionGeometry implements Geometry {
    @JsonProperty private Double x;
    @JsonProperty private Double y;
    @JsonProperty private String type;
    @JsonProperty private Double[][] coordinates;

    @Override
    public Double getX() {
      return x;
    }

    @Override
    public Double getY() {
      return y;
    }
  }
}
