# Welcome to Stratosphere

Stratosphere is the open-source CloudOps companion.  It aims to be an easy-to-use and friendly
tool.

## Building
Building stratosphere is easy!
### Step 1: Install NVM

NVM, or Node Version Manager, is a script to help you manage nodejs versions. Follow the instructions
at [Installing NVM](https://github.com/nvm-sh/nvm#installing-and-updating).
At sunshower.io, we use
1. Node version v18.17.1 (LTS), which may be installed via `nvm i v18.17.1`
2. NPM 9.6.7 (will come with node)

### Step 2: Install SDKMAN (optional)
You can install Java, Gradle, and Maven through other means, but [SDKMAN](https://sdkman.io) is what we use.  Head on over there, or execute (if you're brave) `curl -s "https://get.sdkman.io" | bash` (or zsh, or whatever).

We assume that if you're not using SDKMAN you know what you're doing!


### Install Java
Once SDKMAN's installed, install java 17.  It doesn't really matter which flavor you use (probably), but we like Zulu, which can be installed via:

```bash
sdk install java 17.0.8-zulu
```

### Install Maven 
Install Maven 3.9.+ via 
```bash
sdk install maven 3.9.3
```


### Install Gradle
Install Gradle 7.6.x 
```bash
sdk install gradle 7.6.2 
```


## Building the project

1. Install the Stratosphere Bill-of-Materials POM: `mvn clean install -f bom`
2. Install the project: `gradle clean build`
3. Run the project: `gradle bootRun`


Stratosphere is currently based on Java 17, Spring Boot 3, and Vaadin 24.  These versions are managed in our POM [Bill of Materials POM](./bom/imported/pom.xml)



