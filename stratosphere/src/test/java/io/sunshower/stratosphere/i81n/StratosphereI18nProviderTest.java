package io.sunshower.stratosphere.i81n;

import static org.junit.jupiter.api.Assertions.*;

import com.vaadin.flow.i18n.I18NProvider;
import io.sunshower.stratosphere.configuration.StratosphereConfiguration;
import jakarta.inject.Inject;
import java.util.Locale;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = StratosphereConfiguration.class)
class StratosphereI18nProviderTest {

  @Inject private I18NProvider provider;

  @Test
  void ensureProviderIsInjected() {
    assertNotNull(provider);
  }

  @Test
  void ensureMessageIsCorrect() {
    assertEquals(provider.getTranslation("i18n:cards.topography.title", Locale.US), "Global View");
  }

  @Test
  void ensureLocalesIsCorrect() {
    assertEquals(1, provider.getProvidedLocales().size());
  }
}
