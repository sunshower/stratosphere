package io.sunshower.stratosphere.test;

import com.aire.ux.test.AireTest;
import io.sunshower.stratosphere.StratosphereApplication;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;

@AireTest
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@SpringBootTest(classes = StratosphereApplication.class)
public @interface StratosphereTest {}
