package io.sunshower.stratosphere.ui.components.globe;

import static org.junit.jupiter.api.Assertions.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.sunshower.stratosphere.api.cloud.geo.PathFeature;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PathFeatureTest {

  private ObjectMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new ObjectMapper();
  }

  @Test
  @SneakyThrows
  void ensureObjectMapperReadsFeature() {
    val result =
        mapper
            .readerFor(PathFeature.class)
            .<PathFeature>readValue(
                Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream(
                        "META-INF/resources/assets/features/location-pin.feature"));
    assertNotNull(result.getPath());
  }
}
