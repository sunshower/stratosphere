package io.sunshower.stratosphere.ui.views;

import static org.junit.jupiter.api.Assertions.*;

import com.aire.ux.test.*;
import io.sunshower.stratosphere.test.StratosphereTest;
import io.sunshower.stratosphere.ui.components.Card;
import java.util.List;

@StratosphereTest
@RouteLocation(scanClassPackage = MainView.class)
class MainViewTest {
  @ViewTest
  void ensureViewComponentIsLoaded(@Context TestContext $, @Select MainView view) {
    assertNotNull(view);
  }

  @ViewTest
  void ensureCardsAreResolvable(@Select("stratosphere-card") List<Card> cards) {
    assertFalse(cards.isEmpty());
  }
}
