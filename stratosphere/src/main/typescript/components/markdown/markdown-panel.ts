import {css, html, LitElement} from "lit";
import {customElement} from "lit/decorators.js";
import {unsafeHTML} from 'lit/directives/unsafe-html.js'

import * as showdown from 'showdown';

@customElement('markdown-panel')
export class MarkdownPanel extends LitElement {

    static styles = css`
        div.content {
          padding: 1em;
          min-width: 300px;
        }
    `;
    private converter: showdown.Converter;
    private text: string;
    private markdown: string;


    public constructor() {
        super();
        this.converter = new showdown.Converter();
        this.converter.setFlavor('github');
    }

    protected render(): unknown {
        return html`<div hidden>
            <slot @slotchange=${this.updateText}></slot>
        </div>
        <div part="content" class="content">${unsafeHTML(this.markdown)}</div>
        `
    }

    connectedCallback() {
        super.connectedCallback();
        this.markdown = this.converter.makeHtml(this.text);
        this.requestUpdate();
    }

    updateText(e: any) {
        this.text = (e.target as any)
            .assignedNodes()
            .map((n:Node) => n.textContent)
            .join("");

    }
}