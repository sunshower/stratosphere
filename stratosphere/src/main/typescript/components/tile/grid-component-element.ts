import {customElement, query} from "lit/decorators.js";
import {css, html, LitElement, PropertyValues} from "lit";
import {queryAll, queryAsync} from "lit-element";

type State = 'open' | 'closed';


@customElement('grid-component')
export class GridComponentElement extends LitElement {

    @query('.grid-element')
    private elements: Array<Element> | undefined;

    protected render(): unknown {
        return html`
            <slot></slot>
        `
    }

    constructor() {
        super();
    }

}
