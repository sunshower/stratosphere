import {customElement,
    property, query
} from "lit/decorators.js";
import {css, html, LitElement} from "lit";



@customElement('stratosphere-card')
export class CardElement extends LitElement {

    @property({
        attribute: true
    })
    private loading: boolean;


    static styles = css`
      :host {
        position: relative;
      }
      section {
        position: relative;
      }
      div.overlay {
        visibility: collapse;
      }

      div.overlay.loading {
        visibility: visible;
        position: absolute;
        top: calc(1 * var(--sunshower-em));
        left: var(--sunshower-em);
        bottom: calc(1 * var(--sunshower-em));
        right: var(--sunshower-em);
        background-color: white;
        opacity: 0.6;
        z-index: 100;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      div.overlay.loading i {
        width:32px;
        height:32px;
      }
    `;


    @query("slot[name=contents]")
    private contents: HTMLSlotElement;

    @query('div')
    private overlay: HTMLDivElement;

    protected render(): unknown {
        return html`
            <header part="header">
                <h1 part="title">${this.title}</h1>
                <slot part="toolbar" name="toolbar"></slot>
            </header>
            <section part="contents">
                <slot name="contents"></slot>
                <div class="overlay">
                    <slot name="spinner"></slot>
                </div>
                </section>
            </section>
        `
    }

    public setLoading(loading: boolean) : void {
        const overlay = this.overlay,
            contents = this.contents;
        console.log("LOADING", loading);
        if(contents && overlay) {
            if(loading) {
                overlay.classList.add('loading')
            } else {
                overlay.classList.remove('loading');
            }
            this.requestUpdate();
        }
    }

    connectedCallback() {
        this.setLoading(this.loading);
        super.connectedCallback();
    }


    attributeChangedCallback(name: string, _old: string | null, value: string | null) {
        if(name && name === 'loading') {
            this.setLoading(value === 'true');
        }
        super.attributeChangedCallback(name, _old, value);
    }
}
