import {customElement,
    property, query
} from "lit/decorators.js";
import {css, html, LitElement} from "lit";



@customElement('stratosphere-card-group')
export class CardGroupElement extends LitElement {


    protected render(): unknown {
        return html`
        <figure part="contents">
            <header part="header">
                <h1 part="title">
                    <slot name="title"></slot>
                </h1>
                <h2 part="subtitle">
                    <slot name="subtitle"></slot>
                </h2>
            </header>
            <section>
                <slot><slot>
            </section>
        </figure>
        `
    }

}
