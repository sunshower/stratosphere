import {customElement, property, query} from "lit/decorators.js";
import {css, html, LitElement} from "lit";

type State = 'open' | 'closed';


@customElement('contextual-help-element')
export class ContextualHelpElement extends LitElement {

    static styles = css`

      article > section.help, 
      article > section.help[data-state="open"] {
        width: 400px;
        right: 0;
        position: absolute;
      }

      article > section.help[data-state="closed"],
      article > section.help[data-state="open"] {

        transition: 1s;
      }
     
      article > section.help[data-state="closed"] {
        visibility: hidden;
        width: 0;
      }
      
      article > section.help > header {
        border-bottom: 1px solid var(--lumo-contrast-10pct);
        background-color: white;
      }

      article > section.help > header > vaadin-button {
        margin-left: 0.5em;
      }
      article > section.help[data-state="open"] {
        visibility: visible;
      }
      article > div.overlay[data-state="open"] {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background-color: var(--lumo-contrast-50pct);
        transition: 1s;
      }
      article > div.overlay[data-state="closed"] {
        visibility: hidden;
        transition: 1s;
      }
    `;

    @property({
        attribute: 'data-state'
    })
    private state: State = 'closed';

    @query('article > section.body')
    private body: HTMLElement;

    @query('article > section.help')
    private help: HTMLElement;

    @query('article > div.overlay')
    private overlay: HTMLDivElement;



    protected render(): unknown {
        return html`
            <article part="main">
                <div class="overlay" data-state="${this.state}"></div>
                <section part="body" class="body">
                    <slot name="body">
                    </slot>
                </section>
                <section part="help" class="help" data-state="${this.state}">
                    <header>
                        <vaadin-button
                                aria-label="Close Contextual Help"
                                class="closeButton"
                                part="close"
                                theme="tertiary"
                        >
                            <vaadin-icon
                                    icon="vaadin:close-big"
                                    @click="${this.close}">
                            </vaadin-icon>
                        </vaadin-button>
                    </header>
                    <slot name="help"></slot>
                </section>
            </article>`;
    }


    connectedCallback() {
        super.connectedCallback();
    }



    public open() : void {
        this.state = 'open';
    }
    public close() : void {
        this.state = 'closed'
    }
    public toggleHelp() {
        if(this.state === 'open') {
            this.close();
        } else {
            this.open();
        }
    }

}