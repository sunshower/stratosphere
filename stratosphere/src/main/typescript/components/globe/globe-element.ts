import {
    query,
    property,
    customElement,
} from "lit/decorators.js";

import {
    html,
    css,
    LitElement,
} from "lit";

import * as d3 from 'd3';
import * as topojson from 'topojson-client';
import {stackOrderInsideOut} from "d3";

type Point = {
    readonly type: string,
    readonly coordinates: [number, number];
}

type Feature = {
    x: number,
    y: number,
    name: string,
    path: string
}

@customElement('globe-element')
export class GlobeElement extends LitElement {

    @property({
        attribute: 'expand',
        reflect: true
    })
    private expand: boolean;

    @property({
        attribute: true
    })
    private width: string;

    @property({
        attribute: true
    })
    private height: string;

    @property({
        attribute: true
    })
    private scale: number;

    @property({
        attribute: true
    })
    private rotatable: boolean;

    @property({
        attribute: true
    })
    private zoomable: boolean;

    @property({
        attribute: true
    })
    private sensitivity: number;

    @property({
        attribute: 'minimum-scale'
    })
    private minimumScale: number;

    @property({
        attribute: 'hover-color'
    })
    private hoverColor: string;


    @property({
        attribute: 'stroke-color'
    })
    private strokeColor: string;


    @property({
        attribute: 'fill-color',
    })
    private fillColor: string;

    static styles = css`
      svg {
        width: 100%;
        height: 100%;
      }
    `

    private svg: d3.Selection<
        any,
        any,
        any,
        any> | undefined;

    @query('div > svg')
    private canvas: SVGElement | undefined;
    private projection: d3.GeoProjection | undefined;

    private readonly points: Array<Point>;
    private path: d3.GeoPath<any, d3.GeoPermissibleObjects>;
    private currentScale: number | undefined;


    constructor() {
        super();
        this.points = [];
        this.width = '100%';
        this.height = '100%';
        this.rotatable = true;
        this.zoomable = true;
        this.sensitivity = 75;
        this.minimumScale = 0.3;
        this.strokeColor = 'white';
        this.fillColor = 'white';
        this.hoverColor = '#660066';
        this.scale = parseInt(this.width, 10);
    }


    connectedCallback() {
        this.redraw(true);
        super.connectedCallback();
    }





    private async redraw(load: boolean) {

        let
            expand = this.expand,
            tscale =  expand ? this.scale + 14 : this.scale / 2,
            width = expand ? this.clientWidth : parseInt(this.width, 10)
            , height = expand? this.clientHeight : parseInt(this.height, 10)
            , svg = d3.select(this.canvas as any)
            , projection = this.getProjection(width, height, tscale)
            , path = d3.geoPath().projection(projection)
            , scale = this.scale = projection.scale()
            , globe = this.createGlobe(svg, width, height, scale);

        path = this.attachDragBehavior(svg, projection, path);
        path = this.attachZoomBehavior(svg, projection, path, globe);
        this.svg = svg;
        this.path = path;
        this.projection = projection;
        if(load) {
            await this.loadData(svg, path, projection);
        }

    }


    private getProjection(width: number, height: number, scale: number) {
        return d3
            .geoOrthographic()
            .center([0, 0])
            .translate([
                width / 2,
                height / 2
            ])
            .scale(scale);
    }

    private attachZoomBehavior(
        svg: d3.Selection<any, any, any, any>
        , projection: d3.GeoProjection
        , path: d3.GeoPath<any, any>
        , globe: d3.Selection<any, any, any, any>
    ) {
        svg.call(d3.zoom().on('zoom', (event) => {
            if (event.transform.k > 0.3) {
                projection.scale(this.scale * event.transform.k);
                path = d3.geoPath().projection(projection);
                svg.selectAll("path")
                    .attr('transform', (d: any) => {
                        if (d.path) {
                            let scale = this.currentScale = event.transform.k;
                            const [x, y] = projection([d.x, d.y]);
                            return `translate(${x}, ${y}) scale(${scale})`
                        } else {
                            return null;
                        }
                    })
                    .attr("d", (d: any) => {
                        if (d.path) {
                            return d.path;
                        } else {
                            return path(d);
                        }
                    });
                globe.attr("r", projection.scale());
            } else {
                event.transform.k = 0.3;
            }
        }));
        svg.on('click', e => {
            this.doDispatch(e, 'globe-element:globe:clicked');
        });
        return path;
    }


    public addFeatures(features: string) : void {
        if(features) {
            const fs = JSON.parse(features) as Feature[]
            if(!fs.length) {
                return;
            }
            fs.forEach(f => this.doAddFeature(f));
            if (fs && fs.length) {
                const feature = fs[0]//fs[fs.length - 1];
                this.center(feature.x, feature.y);
            }
        }
    }

    public addFeature(fs: string) {
        const feature = JSON.parse(fs) as Feature;
        this.doAddFeature(feature);
    }

    public center(x: number, y: number) : void {
        console.log(x, y);
       let
           speed = 5,
            projection = this.projection,
            cx = x,
            cy = y,
            dx = x >= 0 ? -1 : 1 ,
            dy =  y >= 0 ? -1 : 1,
            cmx = x < 0 ? (
                a:number,
                b: number
            ) => a > b : (
                a:number,
                b:number
            ) => a < b,
            cmy= y < 0 ? (
                a:number,
                b:number
            ) => a > b : (
                a:number,
                b:number
            ) => a < b,
            timer = d3.timer((elapsed) => {
                projection.rotate([ cx += (dx * speed), cy += (dy * speed)]);
                if(cmx(cx, -x)) {
                    dx = 0;
                }
                if(cmy(cy, -y)) {
                    dy = 0;
                }
                if(dy === 0 && dx === 0) {
                    timer.stop();
                }
                this.doRedraw(projection, this.svg, this.path);
            });
    }

    protected render(): unknown {
        return html`
            <div style="width: ${this.width}; height: ${this.height}">
                <svg></svg>
            </div>
        `
    }

    private createGlobe(
        svg: d3.Selection<any, any, any, any>,
        width: number,
        height: number,
        scale: number
    ) {
        this.createGlow(svg);
        return svg.append('circle')
            .attr('fill', '#FAFAFA')
            .attr('stroke', '#000')
            .attr('stroke-width', '0.2')
            .attr('cx', width / 2)
            .attr('cy', height / 2)
            .attr('r', scale);
    }

    private createGlow(svg: d3.Selection<any, any, any, any>) : void {
        const defs = svg.append('defs'),
            filter = defs.append('filter');
        filter
            .attr('id', 'glow')
            .append('feGaussianBlur')
            .attr('class', 'blur')
            .attr('result', 'coloredBlur')
            .attr('stdDeviation', '5');
        const merge = filter.append('feMerge');
        merge
            .append('feMergeNode')
            .attr('in', 'coloredBlur');
        merge.append('feMergeNode')
            .attr('in', 'coloredBlur');
       merge
            .append('feMergeNode')
            .attr('in', 'coloredBlur');
       merge
            .append('feMergeNode')
            .attr('in', 'SourceGraphic')
    }

    private attachDragBehavior(
        svg: d3.Selection<
            any,
            any,
            any,
            any
        >
        , projection: d3.GeoProjection
        , path: d3.GeoPath<
            any,
            any
        >) {
        if (this.rotatable) {
            svg.call(d3.drag().on('drag', (event) => {
                const rotate = projection.rotate();
                const k = this.sensitivity / projection.scale();
                projection.rotate([
                    rotate[0] + event.dx * k,
                    rotate[1] - event.dy * k
                ]);
                path = d3.geoPath().projection(projection);
                this.doRedraw(projection, svg, path);
            }));
        }
        return path;
    }

    private doRedraw(projection: d3.GeoProjection, svg: d3.Selection<any, any, any, any>, path: d3.GeoPath<any, any>) : void {
        svg.selectAll('path')
            .attr('transform', (d: any) => {
                if (d.name) {
                    let [x, y] =
                        projection([d.x, d.y]);
                    return `translate(${x}, ${y}) scale(${this.currentScale || 1})`;
                }
                return null;
            })
            .attr('visibility', (d: any) => {
                if (d.path) {
                    return path({
                            type: 'Point'
                            , coordinates: [d.x, d.y]
                        }
                    ) ? 'visible' : 'hidden';
                }
                return 'visible';
            })
            .attr('d', (d: any) => {
                if (d.path) {
                    return d.path;
                } else {
                    return path(d);
                }
            });

    }

    private async loadData(
        svg: d3.Selection<any, any, any, any>
        , path: d3.GeoPath<any, any>
        , projection: d3.GeoProjection
    ) {
        const worldData = await d3.json('/assets/topology/countries/geo-with-states.json') as any,
            countries = (topojson.feature(
                worldData,
                worldData.objects.countries
            ) as any)
                .features
                .map((f: any) => this.objectType(f, 'c')),
            states = (
                topojson.feature(
                    worldData,
                    worldData.objects.states) as any
            ).features
                .map((f: any) => this.objectType(f, 's')),
            all = states.concat(countries);


        svg.append("g")
            .selectAll("path")
            .data(all)
            .enter()
            .append("path")
            .attr('class', 'boundary')
            .attr('fill', '#ECECEC')
            .attr('stroke', '#660066')
            .attr('stroke-width', (node: any) => {
                // return node.objectType === 's' ? '0.2px' : '0.6px';
                return '0.1px'
            })
            .attr('filter', 'url(#glow)')
            .on('mouseenter', this.onHover('path:enter', projection))
            .on('mouseleave', this.onLeave('path:leave', projection))
            .on('click', this.dispatch('globe-element:path:clicked'))
            .attr("d", path as any);
        this.dispatchEvent(new CustomEvent('globe:ready'));
    }

    private dispatch(name: string): (event: MouseEvent) => void {
        return e => {
            const element = (e.target as Element),
                data = (element as any).__data__;
            this.doDispatch(e, name, {
                id: data.id,
                type: data.type,
                name: data.properties.name,
            });
        }
    }


    private onLeave(name: string, projection: d3.GeoProjection): (event: MouseEvent) => void {
        return e => {
            (e.target as Element).setAttribute('stroke-width', '.1px');
        }
    }

    private onHover(name: string, projection: d3.GeoProjection): (event: MouseEvent) => void {
        return (e: Event) => {
            (e.target as Element).setAttribute('stroke-width', '.4px');
            (e.target as Element).setAttribute('z-index', '100');
        }
    }

    private objectType(t: any, type: string): any {
        t.objectType = type;
        return t;
    }

    private doDispatch(e: Event, name: string, value?: Object) {
        const event = e as MouseEvent,
            projection = this.projection,
            [x, y] = projection.invert([
                event.x - this.offsetLeft - 10
                , event.y - this.offsetTop - 10
            ]),
            coords = {
                x: x,
                y: y,
            },
            detail = value ? {
                coords,
                ...value
            } : coords;

        this.dispatchEvent(new CustomEvent(name, {
            bubbles: false,
            detail: detail
        }))
    }

    private doAddFeature(feature: Feature) {
        let
            projection = this.projection;
        if (projection) {
            this.svg.selectAll('g')
                .append("path")
                .datum(feature)
                .attr('transform', (d) => {
                    let [px, py] = projection([
                        d.x,
                        d.y
                    ]);
                    return `translate(${px}, ${py}) scale(${this.currentScale || 1})`
                })
                .attr('class', 'boundary')
                .attr("d", (d) => {
                    return d.path;
                })
                .attr("fill", "lightblue")
                .attr('stroke-width', 1)
                .attr('stroke', '#66066')
        }
    }
}
