package io.sunshower.stratosphere;

import io.sunshower.stratosphere.configuration.StratosphereConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(StratosphereConfiguration.class)
public class StratosphereApplication {

  static {
    System.setProperty("java.awt.headless", "false");
  }

  public static void main(String[] args) {
    SpringApplication.run(StratosphereApplication.class, args);
  }
}
