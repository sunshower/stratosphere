package io.sunshower.stratosphere.ui.components.markdown;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;

@Tag("markdown-panel")
@JsModule("ts/components/markdown/markdown-panel.js")
@NpmPackage(value = "showdown", version = "2.1.0")
public class MarkdownPanel extends HtmlContainer {

  public void setContent(String s) {
    this.setText(s);
  }
}
