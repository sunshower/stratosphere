package io.sunshower.stratosphere.ui.components.globe;

import com.vaadin.flow.component.DomEvent;

@DomEvent(value = AbstractGlobeEvent.EventNames.GLOBE_READY)
public class GlobeReadyEvent extends GlobeLifecycleEvent {

  public GlobeReadyEvent(GlobeElement source, boolean fromClient) {
    super(source, fromClient, null);
  }
}
