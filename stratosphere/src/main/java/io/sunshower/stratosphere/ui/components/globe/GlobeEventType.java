package io.sunshower.stratosphere.ui.components.globe;

import io.sunshower.stratosphere.ui.events.EventRegistry;
import io.sunshower.stratosphere.ui.events.KeyedEvent;

public enum GlobeEventType implements KeyedEvent {
  GlobeReady(AbstractGlobeEvent.EventNames.GLOBE_READY),
  GlobeClicked(AbstractGlobeEvent.EventNames.GLOBE_CLICKED);

  private final String key;

  static {
    EventRegistry.register(PathEventType.class);
  }

  GlobeEventType(String key) {
    this.key = key;
  }

  @Override
  public String getKey() {
    return key;
  }
}
