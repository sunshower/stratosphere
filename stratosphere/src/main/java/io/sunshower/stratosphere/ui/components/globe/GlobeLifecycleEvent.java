package io.sunshower.stratosphere.ui.components.globe;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vaadin.flow.component.EventData;
import elemental.json.JsonValue;
import io.sunshower.stratosphere.ui.events.EventRegistry;
import io.sunshower.stratosphere.ui.events.KeyedEvent;

public class GlobeLifecycleEvent
    extends AbstractGlobeEvent<
        GlobeLifecycleEvent.LifecycleDetails, GlobeLifecycleEvent.LifecycleEvent> {

  public enum LifecycleEvent implements KeyedEvent {
    GlobeLoading(AbstractGlobeEvent.EventNames.GLOBE_LOADING),
    GlobeLoaded(AbstractGlobeEvent.EventNames.GLOBE_LOADED),
    GlobeReady(AbstractGlobeEvent.EventNames.GLOBE_READY);

    static {
      EventRegistry.register(LifecycleEvent.class);
    }

    private final String key;

    LifecycleEvent(final String key) {
      this.key = key;
    }

    @Override
    public String getKey() {
      return key;
    }
  }

  public GlobeLifecycleEvent(
      final GlobeElement source, boolean fromClient, @EventData("event.detail") JsonValue details) {
    super(source, fromClient, details, null, LifecycleDetails.class);
  }

  @JsonIgnoreProperties("event-type")
  public static class LifecycleDetails {}
}
