package io.sunshower.stratosphere.ui.core;

import com.vaadin.flow.component.Component;
import jakarta.inject.Inject;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;
import lombok.val;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Service;

@Service
public class WidgetFinder {

  static final String DEFAULT_BASE_PACKAGE;

  static {
    DEFAULT_BASE_PACKAGE = "io.sunshower.stratosphere";
  }

  private final ApplicationContext context;
  private final ClassPathScanningCandidateComponentProvider provider;

  @Inject
  public WidgetFinder(final ApplicationContext context) {
    this.context = context;
    provider = new ClassPathScanningCandidateComponentProvider(false, context.getEnvironment());
    provider.addIncludeFilter(new AnnotationTypeFilter(GridComponent.class));
  }

  public Collection<? extends GridComponentDescriptor<?>> findGridComponents() {
    return findGridComponents(DEFAULT_BASE_PACKAGE);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public Collection<? extends GridComponentDescriptor<?>> findGridComponents(String basePackage) {
    return provider.findCandidateComponents(basePackage).stream()
        .flatMap(
            b -> {
              try {
                val descriptor =
                    componentFrom(
                        (Class<? extends Component>)
                            Class.forName(
                                b.getBeanClassName(),
                                false,
                                Thread.currentThread().getContextClassLoader()));
                return Stream.of(descriptor);
              } catch (Exception ex) {
                return Stream.empty();
              }
            })
        .sorted(
            Comparator.comparingInt(
                (ToIntFunction<GridComponentDescriptor<?>>) GridComponentDescriptor::rank))
        .toList();
  }

  private GridComponentDescriptor<?> componentFrom(Class<? extends Component> type) {
    val annotation = type.getAnnotation(GridComponent.class);
    return new GridComponentDescriptor<>(
        type, annotation.rank(), annotation.cellsWide(), annotation.cellsHigh());
  }
}
