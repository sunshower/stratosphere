package io.sunshower.stratosphere.ui.components.globe;

import io.sunshower.stratosphere.configuration.StratosphereConfiguration;
import java.io.IOException;
import lombok.val;

public class Paths {

  public static <T> T read(String path, Class<T> type) throws IOException {
    val mapper = StratosphereConfiguration.getObjectMapper();
    val contextClassLoader = Thread.currentThread().getContextClassLoader();
    try (val stream = contextClassLoader.getResourceAsStream("META-INF/resources/" + path)) {
      return mapper.readValue(stream, type);
    }
  }
}
