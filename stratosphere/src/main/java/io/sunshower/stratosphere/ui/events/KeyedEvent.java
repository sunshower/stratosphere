package io.sunshower.stratosphere.ui.events;

public interface KeyedEvent {
  String getKey();
}
