package io.sunshower.stratosphere.ui.layouts;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.shared.Registration;

public interface ContextualHelpElement<
    View extends Component, T, U extends Component & ContextualHelpElement<View, T, U>> {

  Registration addContextualHelpEventListener(ContextualHelpEventListener<View, T, U> listener);
}
