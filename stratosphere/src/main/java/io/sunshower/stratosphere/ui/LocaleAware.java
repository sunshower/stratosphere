package io.sunshower.stratosphere.ui;

import com.vaadin.flow.server.VaadinService;
import java.util.Locale;
import lombok.val;

public interface LocaleAware {

  default Locale getPageLocale() {
    val result = VaadinService.getCurrentRequest().getLocale();
    if (result == null) {
      return Locale.US;
    }
    return result;
  }
}
