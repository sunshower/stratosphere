package io.sunshower.stratosphere.ui.views.cards;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.function.ValueProvider;
import io.sunshower.stratosphere.api.cloud.model.compute.ComputeInstance;
import io.sunshower.stratosphere.api.cloud.service.compute.ComputeInstanceService;
import io.sunshower.stratosphere.i81n.I18n;
import io.sunshower.stratosphere.ui.components.Card;
import io.sunshower.stratosphere.ui.core.GridComponent;
import jakarta.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@GridComponent(targetRoles = "main", rank = 1, cellsWide = 5, cellsHigh = 5)
public class InstanceCard extends Card<List<ComputeInstance>> {

  private final Grid<ComputeInstance> instances;
  private final ComputeInstanceService instanceService;

  @Inject
  public InstanceCard(final I18n i18n, final ComputeInstanceService instanceService) {
    this.instanceService = instanceService;
    i18n.context("i18n").localize("cards.instances.title", this::setTitle);
    getClassNames().add("no-pad");

    this.instances = new Grid<>();
    instances.setHeight("unset");
    instances.getStyle().set("border", "none");
    instances.addColumn((ValueProvider<ComputeInstance, String>) ComputeInstance::getInstanceId);
    instances.addColumn((ValueProvider<ComputeInstance, String>) ComputeInstance::getName);
    setContents(instances);
    addRefreshItem(Object::new, Object::new);
  }

  @Override
  protected void beforeReload() {
    instanceService.reload();
  }

  @Override
  protected List<ComputeInstance> loadData() {
    return Optional.ofNullable(instanceService.list()).orElse(Collections.emptyList());
  }

  @Override
  protected void setData(List<ComputeInstance> data) {
    instances.setDataProvider(new ListDataProvider<>(data == null ? new ArrayList<>() : data));
  }
}
