package io.sunshower.stratosphere.ui.components.buttons;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;

public class FloatingActionButton extends Button {

  public FloatingActionButton() {
    super(new Icon(VaadinIcon.PLUS));
    setAriaLabel("Add Infrastructure");
    setClassName("floatingActionButton");
  }
}
