package io.sunshower.stratosphere.ui.layouts;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import lombok.Getter;

@Getter
public class ContextualHelpEvent<
        View extends Component, T, U extends Component & ContextualHelpElement<View, T, U>>
    extends ComponentEvent<U> {

  private final T model;
  private final Class<View> view;

  public ContextualHelpEvent(
      final T model, final U source, final boolean fromClient, final Class<View> view) {
    super(source, fromClient);
    this.model = model;
    this.view = view;
  }

  public ContextualHelpEvent(final T model, final U source, final Class<View> view) {
    this(model, source, true, view);
  }
}
