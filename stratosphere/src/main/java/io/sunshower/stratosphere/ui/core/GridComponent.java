package io.sunshower.stratosphere.ui.core;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@UIScope
@SpringComponent
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface GridComponent {
  String[] targetRoles();

  int rank();

  int cellsWide();

  int cellsHigh();
}
