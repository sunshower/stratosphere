package io.sunshower.stratosphere.ui.events;

import com.fasterxml.jackson.core.JacksonException;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import elemental.json.JsonValue;
import io.sunshower.stratosphere.configuration.StratosphereConfiguration;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.val;

public abstract class AbstractEvent<
        U, // detail type
        T extends Component,
        E extends Enum<E>>
    extends ComponentEvent<T> {

  @Getter(AccessLevel.PUBLIC)
  private final U detail;

  @Getter(AccessLevel.PROTECTED)
  private final E eventType;

  @Getter(AccessLevel.PROTECTED)
  private final Class<U> detailType;

  protected AbstractEvent(
      T source, boolean fromClient, JsonValue detail, E eventType, Class<U> detailType) {
    super(source, fromClient);
    this.detailType = detailType;
    this.eventType = eventType;
    this.detail = unwrap(detail);
  }

  protected U unwrap(JsonValue value) {
    if (value == null) {
      return null;
    }

    val str = value.toJson();
    val mapper = StratosphereConfiguration.getObjectMapper();
    try {
      return mapper.readValue(str, detailType);
    } catch (JacksonException ex) {
      throw new IllegalStateException(ex);
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName()
        + "{"
        + "value="
        + detail
        + ", type="
        + detailType
        + ", eventType"
        + eventType
        + ", detail"
        + detail
        + '}';
  }
}
