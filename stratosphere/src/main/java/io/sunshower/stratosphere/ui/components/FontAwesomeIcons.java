package io.sunshower.stratosphere.ui.components;

import com.vaadin.flow.component.HtmlComponent;
import com.vaadin.flow.component.Tag;
import lombok.val;

public class FontAwesomeIcons {

  public static final FAIconFactory Globe = new FAIconFactory("globe");

  public static final FAIconFactory ArrowsRotate = new FAIconFactory("arrows-rotate");

  public static FAIconFactory Spinner3rd = new FAIconFactory("spinner-third");
  public static FAIconFactory Spinner = new FAIconFactory("spinner");

  public enum Variant {
    Thin("thin");
    private final String variant;

    Variant(String variant) {
      this.variant = variant;
    }
  }

  public static final class FAIconFactory {
    final String name;
    final String[] variant;

    private FAIconFactory(final String name) {
      this(name, null);
    }

    private FAIconFactory(String name, String... variant) {
      this.name = name;
      this.variant = variant;
    }

    public FAIconFactory variant(String... variant) {
      return new FAIconFactory(name, variant);
    }

    public FAIconFactory variant(Variant v) {
      return variant(v.variant);
    }

    public FAIcon create() {
      if (variant == null) {
        return new FAIcon(name);
      } else {
        return new FAIcon(name).variant(variant);
      }
    }
  }

  @Tag("i")
  public static final class FAIcon extends HtmlComponent {

    private FAIcon(String type) {
      getClassNames().add(String.format("fa-%s", type));
    }

    public FAIcon variant(String... vs) {
      for (val v : vs) {
        getClassNames().add(String.format("fa-%s", v));
      }
      return this;
    }

    public FAIcon variant(Variant v) {
      return variant(v.variant);
    }
  }
}
