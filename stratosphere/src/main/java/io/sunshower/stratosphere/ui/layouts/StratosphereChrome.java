package io.sunshower.stratosphere.ui.layouts;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.RouterLayout;
import lombok.val;

// @CssImport(value = "styles/base/stratosphere-chrome.css")
public class StratosphereChrome extends AppLayout implements RouterLayout {

  private final HorizontalLayout headerLayout;

  public StratosphereChrome() {
    headerLayout = new HorizontalLayout();
    headerLayout.getClassNames().set("header", true);

    getElement().getClassList().set("chrome", true);

    val img = new Image();
    img.setSrc("assets/stratosphere/main/sunshower.svg");

    val title = new H1("Stratosphere");

    val search = new TextField();
    Icon icon = new Icon(VaadinIcon.SEARCH);

    search.setSuffixComponent(icon);
    search.setAriaLabel("Search");

    headerLayout.add(img, title, search);
    addToNavbar(headerLayout);
  }
}
