package io.sunshower.stratosphere.ui.components.globe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GlobeEventDetails {

  @JsonProperty private Double x;

  @JsonProperty private Double y;
}
