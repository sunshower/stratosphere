package io.sunshower.stratosphere.ui.components.globe;

import com.fasterxml.jackson.annotation.JsonProperty;
import elemental.json.JsonValue;
import lombok.Getter;
import lombok.ToString;

public class PathEvent extends AbstractGlobeEvent<PathEvent.PathDetails, PathEventType> {

  public PathEvent(GlobeElement source, boolean fromClient, JsonValue value, PathEventType type) {
    super(source, fromClient, value, type, PathDetails.class);
    new Thread(
        () -> {
          //      this.addFeature()

        });
  }

  @Getter
  @ToString
  public static final class PathDetails extends GlobeEventDetails {
    @JsonProperty("id")
    private String id;

    @JsonProperty("type")
    private String type;

    @JsonProperty("name")
    private String name;
  }
}
