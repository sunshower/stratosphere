package io.sunshower.stratosphere.ui.events;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;
import lombok.val;

public class EventRegistry {

  private static final Map<Class<?>, Map<String, KeyedEvent>> events;

  static {
    events = new ConcurrentHashMap<>();
  }

  public static <E extends Enum<E> & KeyedEvent> void register(Class<E> type) {
    if (events.containsKey(type)) {
      return;
    }
    val map = new HashMap<String, KeyedEvent>();
    events.put(type, map);
    val consts = type.getEnumConstants();
    for (val constant : consts) {
      map.put(constant.getKey(), constant);
    }
  }

  @SuppressWarnings("unchecked")
  public static <E extends Enum<E> & KeyedEvent> E locate(String key, Class<E> type) {
    val results = events.get(type);
    if (results == null) {
      throw new NoSuchElementException("No enum class registered: " + type);
    }
    return (E) results.get(key);
  }
}
