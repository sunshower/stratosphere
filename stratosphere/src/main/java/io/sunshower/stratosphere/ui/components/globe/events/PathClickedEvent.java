package io.sunshower.stratosphere.ui.components.globe.events;

import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import elemental.json.JsonValue;
import io.sunshower.stratosphere.ui.components.globe.GlobeElement;
import io.sunshower.stratosphere.ui.components.globe.PathEvent;
import io.sunshower.stratosphere.ui.components.globe.PathEventType;

@DomEvent("globe-element:path:clicked")
public class PathClickedEvent extends PathEvent {
  public PathClickedEvent(
      GlobeElement source, boolean fromClient, @EventData("event.detail") JsonValue value) {
    super(source, fromClient, value, PathEventType.PathClicked);
  }
}
