package io.sunshower.stratosphere.ui.components.globe;

import elemental.json.JsonValue;
import io.sunshower.stratosphere.ui.events.AbstractEvent;

public class AbstractGlobeEvent<T, E extends Enum<E>> extends AbstractEvent<T, GlobeElement, E> {

  static final class Category {
    static final String PATH = "pathevent";
    static final String GLOBE = "globeevent";
  }

  static final class EventNames {

    static final String GLOBE_LOADING = "globe:loading";
    static final String GLOBE_LOADED = "globe:loaded";
    static final String GLOBE_CLICKED = "globe:clicked";
    static final String GLOBE_READY = "globe:ready";
    static final String PATH_ENTER = "path:enter";
    static final String PATH_LEAVE = "path:leave";
    static final String PATH_CLICKED = "path:clicked";
    static final String PATH_ADDED = "path:added";
    static final String PATH_REMOVED = "path:removed";
    static final String PATH_DOUBLE_CLICKED = "path:double-clicked";
  }

  protected AbstractGlobeEvent(
      GlobeElement source, boolean fromClient, JsonValue detail, E type, Class<T> detailType) {
    super(source, fromClient, detail, type, detailType);
  }
}
