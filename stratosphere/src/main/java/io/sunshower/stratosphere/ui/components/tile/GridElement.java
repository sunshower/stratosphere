package io.sunshower.stratosphere.ui.components.tile;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.di.Instantiator;
import io.sunshower.stratosphere.ui.core.GridComponentDescriptor;
import io.sunshower.stratosphere.ui.layouts.ContextualHelpElement;
import io.sunshower.stratosphere.ui.layouts.ContextualHelpEvent;
import io.sunshower.stratosphere.ui.layouts.ContextualHelpEventListener;
import lombok.Getter;
import lombok.Setter;
import lombok.val;

@Tag("grid-component")
@SuppressWarnings("rawtypes")
@JsModule("ts/components/tile/grid-component-element.js")
public class GridElement extends HtmlContainer implements ContextualHelpEventListener {

  public static final class Constants {
    static final int DEFAULT_CELL_WIDTH = 32;
    static final int DEFAULT_CELL_HEIGHT = 32;
  }

  @Setter private boolean open;

  @Setter @Getter private int cellWidth;

  @Setter @Getter private int cellHeight;

  public GridElement() {
    this(Constants.DEFAULT_CELL_WIDTH, Constants.DEFAULT_CELL_HEIGHT);
  }

  public GridElement(final int cellWidth, final int cellHeight) {
    setCellSize(cellWidth, cellHeight);
  }

  public void addGridElement(GridComponentDescriptor<?> descriptor) {
    if (!HasSize.class.isAssignableFrom(descriptor.type())) {
      throw new IllegalArgumentException("Error: cannot add unsizeable element");
    }
    val ui = getUI().orElse(UI.getCurrent());
    val instantiator = Instantiator.get(ui);
    val widget = (HasSize) instantiator.getOrCreate(descriptor.type());
    widget.setMinWidth(String.format("%dpx", cellWidth * descriptor.rowWidth()));
    widget.setMinHeight(String.format("%dpx", cellHeight * descriptor.rowHeight()));
    widget.getElement().getClassList().add("grid-element");
    add((Component) widget);

    if (widget instanceof ContextualHelpElement<?, ?, ?> element) {
      element.addContextualHelpEventListener(this);
    }
  }

  public void setCellSize(int width, int height) {
    this.cellWidth = width;
    this.cellHeight = height;
  }

  public boolean isOpen() {
    return open;
  }

  public void setOpen() {}

  @Override
  public void onContextualHelpEvent(ContextualHelpEvent event) {
    if (!this.isOpen()) {
      this.setOpen();
    }
    this.updateContextView(event);
  }

  private void updateContextView(ContextualHelpEvent event) {}
}
