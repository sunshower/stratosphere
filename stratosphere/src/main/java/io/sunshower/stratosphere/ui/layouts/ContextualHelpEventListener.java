package io.sunshower.stratosphere.ui.layouts;

import com.vaadin.flow.component.Component;

public interface ContextualHelpEventListener<
    View extends Component, T, U extends Component & ContextualHelpElement<View, T, U>> {
  void onContextualHelpEvent(ContextualHelpEvent<View, T, U> event);
}
