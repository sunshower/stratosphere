package io.sunshower.stratosphere.ui.views.cards;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.function.ValueProvider;
import io.sunshower.stratosphere.api.cloud.model.core.Region;
import io.sunshower.stratosphere.api.cloud.service.core.RegionService;
import io.sunshower.stratosphere.i81n.I18n;
import io.sunshower.stratosphere.ui.components.Card;
import io.sunshower.stratosphere.ui.core.GridComponent;
import io.sunshower.stratosphere.ui.core.event.RegionEvents;
import jakarta.inject.Inject;
import java.util.List;
import lombok.val;
import org.springframework.context.event.EventListener;

@GridComponent(targetRoles = "main", rank = 1, cellsWide = 5, cellsHigh = 5)
public class RegionCard extends Card<List<Region>> {

  private final Grid<Region> regions;
  private final RegionService regionService;

  @Inject
  public RegionCard(final I18n i18n, final RegionService regionService) {
    this.regionService = regionService;
    i18n.context("i18n").localize("cards.region.title", this::setTitle);
    this.getClassNames().add("no-pad");
    this.regions = new Grid<>();
    regions.setHeight("unset");
    regions.getStyle().set("border", "none");
    regions.addColumn((ValueProvider<Region, String>) Region::getName);
    regions.addColumn((ValueProvider<Region, String>) r -> r.getProvider().getKey());
    regions.addItemClickListener(
        (ComponentEventListener<ItemClickEvent<Region>>)
            event -> {
              getPublisher().publishEvent(new RegionEvents.RegionClickedEvent(event.getItem()));
            });
    setContents(regions);
    addRefreshItem(Object::new, Object::new);
  }

  @EventListener(classes = RegionEvents.RegionRefreshStartedEvent.class)
  public void onRegionRefreshStarted(RegionEvents.RegionRefreshStartedEvent event) {
    this.reload(true, () -> {});
  }

  @Override
  protected List<Region> loadData() {
    return regionService.list();
  }

  @Override
  protected void setData(List<Region> data) {
    val provider = new ListDataProvider<>(regionService.list());
    regions.setDataProvider(provider);
    provider.refreshAll();
  }
}
