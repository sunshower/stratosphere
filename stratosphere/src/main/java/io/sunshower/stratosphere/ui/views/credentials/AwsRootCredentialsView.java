package io.sunshower.stratosphere.ui.views.credentials;

import com.vaadin.flow.router.Route;
import io.sunshower.stratosphere.ui.layouts.ContextualHelpView;
import io.sunshower.stratosphere.ui.layouts.StratosphereChrome;

// @CssImport(value = "styles/views/main.css")
@Route(value = "aws/credentials", layout = StratosphereChrome.class)
public class AwsRootCredentialsView extends ContextualHelpView {

  public AwsRootCredentialsView() {}
}
