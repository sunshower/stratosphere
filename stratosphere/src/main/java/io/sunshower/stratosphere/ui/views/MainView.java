package io.sunshower.stratosphere.ui.views;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.router.Route;
import io.sunshower.stratosphere.ui.components.buttons.FloatingActionButton;
import io.sunshower.stratosphere.ui.components.tile.GridElement;
import io.sunshower.stratosphere.ui.core.WidgetFinder;
import io.sunshower.stratosphere.ui.core.WidgetHost;
import io.sunshower.stratosphere.ui.layouts.ContextualHelpView;
import io.sunshower.stratosphere.ui.layouts.StratosphereChrome;
import jakarta.inject.Inject;
import lombok.val;

@WidgetHost("main")
@Route(value = "", layout = StratosphereChrome.class)
public class MainView extends ContextualHelpView {

  private final GridElement layout;
  private final WidgetFinder finder;

  @Inject
  public MainView(WidgetFinder finder) {

    this.finder = finder;
    layout = new GridElement();
    setBody(layout);
    initializeComponent();
    val widgets = finder.findGridComponents();
    for (val widget : widgets) {
      layout.addGridElement(widget);
    }
  }

  @Override
  protected void onAttach(AttachEvent attachEvent) {}

  private void initializeComponent() {
    getClassNames().set("main-view", true);
    setWidthFull();
    setHeightFull();
    add(new FloatingActionButton());
  }
}
