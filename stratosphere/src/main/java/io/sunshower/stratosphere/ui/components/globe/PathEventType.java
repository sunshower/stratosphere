package io.sunshower.stratosphere.ui.components.globe;

import io.sunshower.stratosphere.ui.events.EventRegistry;
import io.sunshower.stratosphere.ui.events.KeyedEvent;

public enum PathEventType implements KeyedEvent {
  PathAdded(AbstractGlobeEvent.EventNames.PATH_ADDED),
  PathRemoved(AbstractGlobeEvent.EventNames.PATH_REMOVED),
  PathClicked(AbstractGlobeEvent.EventNames.PATH_CLICKED),
  PathEnter(AbstractGlobeEvent.EventNames.PATH_ENTER),
  PathLeave(AbstractGlobeEvent.EventNames.PATH_LEAVE),
  PathDoubleClicked(AbstractGlobeEvent.EventNames.PATH_DOUBLE_CLICKED);

  private final String key;

  static {
    EventRegistry.register(PathEventType.class);
  }

  PathEventType(String key) {
    this.key = key;
  }

  @Override
  public String getKey() {
    return key;
  }
}
