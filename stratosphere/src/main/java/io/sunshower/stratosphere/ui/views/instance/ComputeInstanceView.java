package io.sunshower.stratosphere.ui.views.instance;

import com.vaadin.flow.component.html.Div;

public class ComputeInstanceView extends Div {

  public ComputeInstanceView() {
    add("Compute instance");
  }
}
