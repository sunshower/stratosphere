package io.sunshower.stratosphere.ui.components;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Span;
import lombok.val;

@Tag("stratosphere-card-group")
@JsModule("ts/components/card/card-group-element.js")
public class CardGroup extends HtmlContainer {

  public CardGroup() {}

  public void setTitle(String title) {
    val text = new Span(title);
    text.getElement().setAttribute("slot", "title");
    add(text);
  }

  public void setSubtitle(String subtitle) {
    val text = new Span(subtitle);
    text.getElement().setAttribute("slot", "subtitle");
    add(text);
  }

  public void addCard(Card card) {
    add(card);
  }
}
