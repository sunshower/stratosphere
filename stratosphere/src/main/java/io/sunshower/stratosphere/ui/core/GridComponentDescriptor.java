package io.sunshower.stratosphere.ui.core;

import com.vaadin.flow.component.Component;

/**
 * @param type
 * @param rowWidth
 * @param rowHeight
 * @param <T>
 */
public record GridComponentDescriptor<T extends Component>(
    Class<T> type, int rank, int rowWidth, int rowHeight) {}
