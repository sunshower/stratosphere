package io.sunshower.stratosphere.ui.components;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class Tile extends VerticalLayout {

  private final String title;
  private final Object body;

  public Tile(String title, Object body) {
    this.title = title;
    this.body = body;
    add(new Span(title));
    add(new H1(String.valueOf(body)));

    getStyle().set("border", "1px solid #F2F2F2");
  }
}
