package io.sunshower.stratosphere.ui.components.globe.events;

import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import elemental.json.JsonValue;
import io.sunshower.stratosphere.ui.components.globe.GlobeElement;
import io.sunshower.stratosphere.ui.components.globe.GlobeEvent;
import io.sunshower.stratosphere.ui.components.globe.GlobeEventType;

@DomEvent("globe-element:globe:clicked")
public class GlobeClickedEvent extends GlobeEvent {

  public GlobeClickedEvent(
      GlobeElement source, boolean fromClient, @EventData("event.detail") JsonValue detail) {
    super(source, fromClient, detail, GlobeEventType.GlobeClicked);
  }
}
