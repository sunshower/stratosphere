package io.sunshower.stratosphere.ui.components;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;

@Tag("stratosphere-card")
@JsModule("ts/components/card/card-element.js")
public abstract class Card<T> extends HtmlContainer implements ApplicationContextAware {

  @Getter private final HasComponents toolbar;

  private final AtomicBoolean loading;
  private ExecutorService executorService;
  private ApplicationEventPublisher publisher;
  private Component contents;

  public Card() {
    toolbar = createToolbar();
    add((Component) toolbar);
    loading = new AtomicBoolean(false);
    val spinner = FontAwesomeIcons.Spinner.variant("solid", "spin", "xl").create();
    setSpinner(spinner);
  }

  public void setSpinner(Component spinner) {
    spinner.getElement().setAttribute("slot", "spinner");
    add(spinner);
  }

  public void setContents(Component contents) {
    if (this.contents != null) {
      remove(this.contents);
    }
    this.contents = contents;
    contents.getElement().setAttribute("slot", "contents");
    add(contents);
  }

  @Override
  protected void onAttach(AttachEvent attachEvent) {
    reload(false, () -> {});
  }

  protected void setLoading() {
    val ui = getUI().orElse(UI.getCurrent());
    ui.access(
        () -> {
          this.loading.set(true);
          getElement().setAttribute("loading", "true");
          ui.push();
        });
  }

  protected void clearLoading() {
    val ui = getUI().orElse(UI.getCurrent());
    ui.access(
        () -> {
          this.loading.set(false);
          getElement().setAttribute("loading", "false");
          ui.push();
        });
  }

  protected HasComponents createToolbar() {
    val r = new HorizontalLayout();
    r.getElement().setAttribute("slot", "toolbar");
    r.setWidth("100%");
    r.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
    return r;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.publisher = applicationContext;
    this.executorService = applicationContext.getBean(ExecutorService.class);
  }

  protected ApplicationEventPublisher getPublisher() {
    return publisher;
  }

  protected <T, U> void addRefreshItem(
      Supplier<T> createEventStarted, Supplier<U> createEventFinished) {
    val refresh =
        new Button(FontAwesomeIcons.ArrowsRotate.variant(FontAwesomeIcons.Variant.Thin).create());
    refresh.addThemeVariants(
        ButtonVariant.LUMO_TERTIARY_INLINE, ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_ICON);

    refresh.addClickListener(click -> doLoad(refresh, createEventStarted, createEventFinished));
    getToolbar().add(refresh);
  }

  @SneakyThrows
  private <T, U> void doLoad(
      Button refresh, Supplier<T> createEventStarted, Supplier<U> createEventFinished) {
    refresh.setEnabled(false);
    publisher.publishEvent(createEventStarted.get());
    reload(
        true,
        () -> {
          refresh.setEnabled(true);
          publisher.publishEvent(createEventFinished.get());
        });
  }

  protected void reload(boolean runBefore, Runnable after) {
    setLoading();
    CompletableFuture.runAsync(
            () -> {
              if (runBefore) {
                beforeReload();
              }
            },
            executorService)
        .thenApplyAsync(v -> this.raceDelay(), executorService)
        .thenAccept(
            data -> {
              access(() -> this.setData(data));
            })
        .thenRun(this::clearLoading)
        .thenRun(() -> access(after));
  }

  protected void access(Runnable r) {
    getUI()
        .ifPresent(
            ui -> {
              ui.access(
                  () -> {
                    r.run();
                    ui.push();
                  });
            });
  }

  protected void beforeReload() {}

  protected abstract T loadData();

  protected abstract void setData(T data);

  protected ExecutorService getExecutorService() {
    return executorService;
  }

  private T raceDelay() {
    val countdownLatch = new CountDownLatch(2);
    val result = new AtomicReference<T>();
    try {
      executorService.invokeAll(
          List.of(
              () -> {
                try {
                  Thread.sleep(2000);
                } finally {
                  countdownLatch.countDown();
                }
                return null;
              },
              () -> {
                try {
                  result.set(loadData());
                } finally {
                  countdownLatch.countDown();
                }
                return null;
              }));
      countdownLatch.await();
    } catch (InterruptedException ex) {
      ex.printStackTrace();
    }
    return result.get();
  }
}
