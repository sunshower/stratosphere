package io.sunshower.stratosphere.ui.core.event;

import io.sunshower.stratosphere.api.cloud.model.core.Region;
import lombok.Getter;

public class RegionEvents {

  public RegionEvents() {}

  public static class RegionRefreshStartedEvent {}

  public static class RegionRefreshedEvent {}

  public static class RegionClickedEvent {
    @Getter final Region region;

    public RegionClickedEvent(Region region) {
      this.region = region;
    }
  }
}
