package io.sunshower.stratosphere.ui.layouts;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import io.sunshower.stratosphere.ui.LocaleAware;
import io.sunshower.stratosphere.ui.components.markdown.MarkdownPanel;
import io.sunshower.stratosphere.util.help.ContextualHelp;
import java.io.IOException;
import java.util.Locale;
import lombok.NonNull;
import lombok.val;

@Tag("contextual-help-element")
@JsModule("ts/components/contextual-help-element.js")
public class ContextualHelpView extends HtmlContainer implements LocaleAware {

  private MarkdownPanel panel;

  public void setBody(@NonNull Component body) {
    body.getElement().setAttribute("slot", "body");
    add(body);
  }

  public void setHelpContent(@NonNull String helpContent) {
    getUI()
        .orElse(UI.getCurrent())
        .access(
            () -> {
              if (panel != null) {
                remove(panel);
              }
              panel = new MarkdownPanel();
              panel.getElement().setAttribute("slot", "help");
              panel.setContent(helpContent);
              add(panel);
            });
  }

  public void openContextualHelp(Locale locale, String path) {
    try {
      val help = ContextualHelp.getContextualHelp(locale, path);
      setHelpContent(help);
    } catch (IOException ex) {
    }
  }

  @Override
  protected void onAttach(AttachEvent attachEvent) {
    openContextualHelp(getPageLocale(), "aws/credentials/why-do-we-need-credentials.md");
  }
}
