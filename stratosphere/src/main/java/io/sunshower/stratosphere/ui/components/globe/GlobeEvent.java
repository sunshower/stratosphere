package io.sunshower.stratosphere.ui.components.globe;

import elemental.json.JsonValue;

public class GlobeEvent extends AbstractGlobeEvent<GlobeEvent.GlobeDetails, GlobeEventType> {

  protected GlobeEvent(
      GlobeElement source, boolean fromClient, JsonValue detail, GlobeEventType type) {
    super(source, fromClient, detail, type, GlobeDetails.class);
  }

  public static class GlobeDetails extends GlobeEventDetails {}
}
