package io.sunshower.stratosphere.ui.views.cards;

import io.sunshower.stratosphere.api.cloud.geo.Feature;
import io.sunshower.stratosphere.api.cloud.service.core.VirtualPrivateCloudService;
import io.sunshower.stratosphere.i81n.I18n;
import io.sunshower.stratosphere.ui.components.Card;
import io.sunshower.stratosphere.ui.components.globe.GlobeElement;
import io.sunshower.stratosphere.ui.core.GridComponent;
import io.sunshower.stratosphere.ui.core.event.RegionEvents;
import jakarta.inject.Inject;
import java.util.List;
import lombok.val;
import org.springframework.context.event.EventListener;

@GridComponent(targetRoles = "main", rank = 0, cellsWide = 5, cellsHigh = 5)
public class TopographyCard extends Card<List<Feature>> {

  private final GlobeElement element;
  private final VirtualPrivateCloudService service;

  @Inject
  public TopographyCard(I18n i18n, VirtualPrivateCloudService service) {
    this.service = service;
    this.element = new GlobeElement();
    element.setExpand(true);
    i18n.context("i18n").localize("cards.topography.title", this::setTitle);
    setContents(element);
    addRefreshItem(
        RegionEvents.RegionRefreshStartedEvent::new, RegionEvents.RegionRefreshedEvent::new);
  }

  @EventListener(classes = RegionEvents.RegionClickedEvent.class)
  public void onRegionClicked(RegionEvents.RegionClickedEvent event) {
    val region = event.getRegion();
    getUI()
        .ifPresent(
            ui -> {
              element.center(region.getGeometry().getX(), region.getGeometry().getY());
              ui.push();
            });
  }

  @Override
  protected void beforeReload() {
    service.reload();
  }

  @Override
  protected List<Feature> loadData() {
    return service.loadFeatures();
  }

  @Override
  protected void setData(List<Feature> data) {
    element.addFeatures(data);
  }
}
