package io.sunshower.stratosphere.ui.components.globe;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.shared.Registration;
import io.sunshower.stratosphere.api.cloud.geo.Feature;
import io.sunshower.stratosphere.configuration.StratosphereConfiguration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import lombok.SneakyThrows;
import lombok.val;

@Tag("globe-element")
@JsModule("ts/components/globe/globe-element.js")
@NpmPackage(value = "d3", version = "7.8.5")
@NpmPackage(value = "lit-element", version = "3.3.3")
@NpmPackage(value = "topojson-client", version = "3.1.0")
@NpmPackage(value = "@aire-ux/aire-condensation", version = "0.1.7")
public class GlobeElement extends HtmlContainer {

  private final UI ui;

  enum State {
    Initializing,
    Ready
  }

  private final AtomicReference<State> state;
  private final List<Feature> pendingFeatures;

  public GlobeElement() {
    this.ui = UI.getCurrent();
    pendingFeatures = new ArrayList<>();
    state = new AtomicReference<>(State.Initializing);
    addGlobeLifecycleEvent(
        GlobeReadyEvent.class,
        e -> {
          setState(State.Ready);
          doInitialize();
        });
  }

  public void setExpand(boolean expand) {
    getElement().setAttribute("expand", String.valueOf(expand));
  }

  public boolean isExpand() {
    return Boolean.parseBoolean(getElement().getAttribute("expand"));
  }

  private void setState(State state) {
    this.state.set(state);
  }

  public <T extends PathEvent> Registration addPathEventListener(
      Class<T> pathEvent, ComponentEventListener<T> listener) {
    return addListener(pathEvent, listener);
  }

  public <T extends GlobeLifecycleEvent> Registration addGlobeLifecycleEvent(
      Class<T> type, ComponentEventListener<T> listener) {
    return addListener(type, listener);
  }

  @SneakyThrows
  public void addFeature(Feature f, Double x, Double y) {
    f.setX(x);
    f.setY(y);
    if (state.get() == State.Ready) {
      val mapper = StratosphereConfiguration.getObjectMapper();
      getElement().callJsFunction("addFeature", mapper.writer().writeValueAsString(f));
    } else {
      pendingFeatures.add(f);
    }
  }

  @SneakyThrows
  public void addFeatures(List<Feature> f) {
    if (state.get() == State.Ready) {
      val results = new ArrayList<>(pendingFeatures);
      val mapper = StratosphereConfiguration.getObjectMapper();
      results.addAll(f);
      getElement().callJsFunction("addFeatures", mapper.writer().writeValueAsString(results));
    } else {
      pendingFeatures.addAll(f);
    }
  }

  public void center(Double x, Double y) {
    getElement().callJsFunction("center", x, y);
  }

  private void doInitialize() {
    addPendingFeatures();
  }

  private void addPendingFeatures() {
    if (!pendingFeatures.isEmpty()) {
      addFeatures(pendingFeatures);
    }
  }
}
