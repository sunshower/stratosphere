package io.sunshower.stratosphere.ui.views.cards;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import io.sunshower.stratosphere.api.cloud.service.compute.ComputeInstanceService;
import io.sunshower.stratosphere.api.cloud.service.core.RegionService;
import io.sunshower.stratosphere.api.cloud.service.core.VirtualPrivateCloudService;
import io.sunshower.stratosphere.i81n.I18n;
import io.sunshower.stratosphere.ui.components.Card;
import io.sunshower.stratosphere.ui.components.Tile;
import io.sunshower.stratosphere.ui.core.GridComponent;
import jakarta.inject.Inject;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import lombok.SneakyThrows;
import lombok.val;

@GridComponent(targetRoles = "main", rank = 0, cellsWide = 5, cellsHigh = 5)
public class ComputeSummaryCard extends Card<ComputeSummaryCard.ComputeDetails> {

  private final I18n i18n;
  private final RegionService regionService;
  private final ComputeInstanceService instanceService;
  private final VirtualPrivateCloudService vpcService;

  @Inject
  public ComputeSummaryCard(
      I18n i18n,
      RegionService regionService,
      VirtualPrivateCloudService vpcService,
      ComputeInstanceService instanceService) {
    this.i18n = i18n;
    i18n.context("i18n").localize("cards.compute-summary.title", this::setTitle);

    this.vpcService = vpcService;
    this.regionService = regionService;
    this.instanceService = instanceService;
    addRefreshItem(Object::new, Object::new);
  }

  @Override
  @SneakyThrows
  protected ComputeDetails loadData() {
    val latch = new CountDownLatch(3);
    val result = new ComputeDetails();
    getExecutorService()
        .invokeAll(
            List.of(
                () -> {
                  try {
                    result.instanceCount = instanceService.getCount();
                  } finally {
                    latch.countDown();
                  }
                  return null;
                },
                () -> {
                  try {
                    result.regionCount = vpcService.getCount();
                  } finally {
                    latch.countDown();
                  }
                  return null;
                },
                () -> {
                  try {
                    result.vpcCount = vpcService.getCount();
                  } finally {
                    latch.countDown();
                  }
                  return null;
                }));
    latch.await();
    return result;
  }

  @Override
  protected void setData(ComputeDetails data) {
    val ctx = i18n.context("i18n");
    val layout = new HorizontalLayout();
    layout.add(new Tile(ctx.message("compute-summary.instance-count"), data.instanceCount));
    layout.add(new Tile(ctx.message("compute-summary.vpc-count"), data.vpcCount));
    layout.setSizeFull();

    layout.add(new Tile(ctx.message("compute-summary.region-count"), data.regionCount));
    layout.setSizeFull();
    setContents(layout);
  }

  static class ComputeDetails {
    private int regionCount;
    private int vpcCount;
    private int instanceCount;
  }
}
