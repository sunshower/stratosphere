package io.sunshower.stratosphere.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.i18n.I18NProvider;
import io.sunshower.stratosphere.api.cloud.CloudCoreModuleConfiguration;
import io.sunshower.stratosphere.cloud.aws.AwsModuleConfiguration;
import io.sunshower.stratosphere.i81n.I18n;
import io.sunshower.stratosphere.i81n.StratosphereI18nProvider;
import io.sunshower.stratosphere.launcher.BrowserLauncher;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import lombok.extern.java.Log;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.Async;

@Log
@Configuration
@ComponentScan(basePackages = "io.sunshower.stratosphere")
@Import({AwsModuleConfiguration.class, CloudCoreModuleConfiguration.class})
public class StratosphereConfiguration implements ApplicationListener<ApplicationReadyEvent> {

  private static final AtomicReference<ObjectMapper> objectMapper;

  static {
    objectMapper = new AtomicReference<>();
  }

  public static ObjectMapper getObjectMapper() {
    synchronized (objectMapper) {
      while (objectMapper.get() == null) {
        try {
          objectMapper.wait();
        } catch (InterruptedException ex) {
          throw new IllegalStateException("Error: failed to start");
        }
      }
    }
    return objectMapper.get();
  }

  private static void setObjectMapper(ObjectMapper mapper) {
    synchronized (objectMapper) {
      objectMapper.set(mapper);
      objectMapper.notifyAll();
    }
  }

  @Async
  @Override
  public void onApplicationEvent(ApplicationReadyEvent event) {
    setObjectMapper(event.getApplicationContext().getBean(ObjectMapper.class));
    try {
      BrowserLauncher.main(new String[0]);
    } catch (Exception ex) {
      log.log(Level.WARNING, "Failed to launch browser.  Reason: {0}", ex.getMessage());
    }
  }

  @Bean
  public I18NProvider i18NProvider() {
    return new StratosphereI18nProvider();
  }

  @Bean
  public I18n i18n(I18NProvider provider) {
    return new I18n(provider);
  }
}
