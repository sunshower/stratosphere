package io.sunshower.stratosphere;

import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.shared.ui.Transport;
import com.vaadin.flow.spring.annotation.EnableVaadin;
import com.vaadin.flow.theme.Theme;
import io.sunshower.stratosphere.ui.components.Tile;
import io.sunshower.stratosphere.ui.views.cards.InstanceCard;
import io.sunshower.stratosphere.ui.views.cards.RegionCard;
import io.sunshower.stratosphere.ui.views.cards.TopographyCard;

/**
 * Use the @PWA annotation make the application installable on phones, tablets and some desktop
 * browsers.
 */
@Theme(value = "stratosphere")
@EnableVaadin("io.sunshower.stratosphere.ui")
@Push(value = PushMode.MANUAL, transport = Transport.LONG_POLLING)
@PWA(name = "Project Base for Vaadin", shortName = "Project Base")
@Uses(Tile.class)
@Uses(Grid.class)
@Uses(InstanceCard.class)
@Uses(RegionCard.class)
@Uses(TopographyCard.class)
public class AppShell implements AppShellConfigurator {}
