package io.sunshower.stratosphere.util.help;

import static java.lang.String.format;

import com.vaadin.flow.server.VaadinService;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.NoSuchElementException;
import lombok.val;

public class ContextualHelp {

  public static String readCurrentContextualHelp(String path) throws IOException {
    var result = VaadinService.getCurrentRequest().getLocale();
    if (result == null) {
      result = Locale.US;
    }
    return getContextualHelp(result, path);
  }

  public static String getContextualHelp(Locale locale, String path) throws IOException {
    val classLoader = Thread.currentThread().getContextClassLoader();
    val localizedPath = format("help/%s/%s", locale, path);
    val resource = classLoader.getResourceAsStream(localizedPath);
    if (resource == null) {
      throw new NoSuchElementException(
          format(
              "Error: no contextual help at locale = %s, path = %s (full path: %s)",
              locale, path, localizedPath));
    }
    try (val input = resource) {
      val bytes = input.readAllBytes();
      return new String(bytes, StandardCharsets.UTF_8);
    }
  }
}
