package io.sunshower.stratosphere.core.credentials;

public interface Credentials {

  String getId();
}
