package io.sunshower.stratosphere.core.credentials;

import java.util.List;

public interface CredentialsManager<C extends Credentials> {

  C getDefaultCredentials();

  List<C> getAllCredentials();

  void addCredentials(C credentials);

  void removeCredentials(C credentials);

  void removeCredentials(String key);

  void setDefaultCredentials(C credentials);

  void setDefaultCredentials(String key);
}
