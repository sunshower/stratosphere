package io.sunshower.stratosphere.clouds.aws;

import io.sunshower.stratosphere.core.credentials.CredentialsManager;
import java.util.List;

public class InMemorySingleCredentialsManager implements CredentialsManager<AwsCredentials> {
  @Override
  public AwsCredentials getDefaultCredentials() {
    return null;
  }

  @Override
  public List<AwsCredentials> getAllCredentials() {
    return null;
  }

  @Override
  public void addCredentials(AwsCredentials credentials) {}

  @Override
  public void removeCredentials(AwsCredentials credentials) {}

  @Override
  public void removeCredentials(String key) {}

  @Override
  public void setDefaultCredentials(AwsCredentials credentials) {}

  @Override
  public void setDefaultCredentials(String key) {}
}
