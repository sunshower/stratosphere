package io.sunshower.stratosphere.i81n;

public @interface Localize {
  String value();
}
