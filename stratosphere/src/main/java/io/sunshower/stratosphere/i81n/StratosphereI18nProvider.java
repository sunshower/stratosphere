package io.sunshower.stratosphere.i81n;

import com.vaadin.flow.i18n.I18NProvider;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

@Slf4j
public class StratosphereI18nProvider implements I18NProvider {

  private final Map<String, Bundle> resourceBundles;

  private final AtomicReference<List<Locale>> cache;

  public StratosphereI18nProvider() {
    cache = new AtomicReference<>();
    resourceBundles = new ConcurrentHashMap<>();
  }

  @Override
  public List<Locale> getProvidedLocales() {
    return cache.updateAndGet(
        c -> {
          if (c == null) {
            return loadLocales();
          }
          return c;
        });
  }

  /**
   * The key is of the format {@code filename}.properties
   *
   * @param key translation key
   * @param locale locale to use
   * @param params parameters used in translation string
   * @return
   */
  @Override
  public String getTranslation(String key, Locale locale, Object... params) {
    val bundle = resolveBundle(fileName(key), locale);
    return MessageFormat.format(bundle.getString(propertyName(key)), params);
  }

  ResourceBundle resolveBundle(String key, Locale locale) {
    return resourceBundles.computeIfAbsent(key, k -> loadBundle(k, locale)).bundle;
  }

  private Bundle loadBundle(String key, Locale locale) {
    val classloader = Thread.currentThread().getContextClassLoader();
    val resource =
        classloader.getResource(
            String.format("META-INF/resources/i18n/%s/%s.properties", locale, key));
    if (resource == null) {
      log.warn("No resource bundle with key {} and locale {}. Defaulting to en_US", key, locale);
      return loadBundle(key, Locale.ENGLISH);
    }
    try (val cnx = resource.openStream();
        val bis = new BufferedInputStream(cnx)) {
      return new Bundle(new PropertyResourceBundle(bis));
    } catch (IOException e) {
      log.warn("Error obtaining bundle: {}", e.getMessage());
      throw new IllegalStateException(e);
    }
  }

  private String propertyName(String key) {
    val l = key.split(":");
    if (l.length != 2) {
      throw new IllegalArgumentException(
          "Error: key name must be of the format <file name>:i18nproperty, was" + key);
    }
    return l[1];
  }

  private String fileName(String key) {
    val l = key.split(":");
    if (l.length != 2) {
      throw new IllegalArgumentException(
          "Error: key name must be of the format <file name>:i18nproperty, was" + key);
    }
    return l[0];
  }

  private List<Locale> loadLocales() {
    return Arrays.stream(Locale.getAvailableLocales())
        .filter(this::isAvailable)
        .collect(Collectors.toList());
  }

  private boolean isAvailable(Locale locale) {
    if (locale.getLanguage().isBlank()) {
      return false;
    }
    val cl = Thread.currentThread().getContextClassLoader();
    return cl.getResource(String.format("META-INF/resources/i18n/%s", locale)) != null;
  }

  private static final class Bundle {

    private final ResourceBundle bundle;

    private Bundle(ResourceBundle bundle) {
      this.bundle = bundle;
    }
  }
}
