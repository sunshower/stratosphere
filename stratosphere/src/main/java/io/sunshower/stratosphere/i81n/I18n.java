package io.sunshower.stratosphere.i81n;

import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.server.VaadinService;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Consumer;
import lombok.val;

public class I18n {

  private final Map<String, I18nContext> cache;

  private final I18NProvider provider;

  public I18n(final I18NProvider provider) {
    this.provider = provider;
    this.cache = new HashMap<>();
  }

  public I18nContext context(String ctx) {
    val locale =
        Optional.ofNullable(VaadinService.getCurrentRequest())
            .flatMap(r -> Optional.of(r.getLocale()))
            .orElse(Locale.US);
    return cache.computeIfAbsent(
        ctx,
        k -> new I18nContext(((StratosphereI18nProvider) provider).resolveBundle(ctx, locale)));
  }

  public static final class I18nContext {

    private final ResourceBundle bundle;

    public I18nContext(ResourceBundle bundle) {
      this.bundle = bundle;
    }

    public void localize(String key, Consumer<String> consumer, Object... args) {
      if (args.length == 0) {
        consumer.accept(bundle.getString(key));
      } else {
        consumer.accept(MessageFormat.format(bundle.getString(key), args));
      }
    }

    public String message(String s, Object... args) {
      if (args.length == 0) {
        return bundle.getString(s);
      } else {
        return MessageFormat.format(bundle.getString(s), args);
      }
    }
  }
}
