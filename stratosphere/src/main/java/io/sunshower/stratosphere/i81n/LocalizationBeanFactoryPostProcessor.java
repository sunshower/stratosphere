package io.sunshower.stratosphere.i81n;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class LocalizationBeanFactoryPostProcessor implements BeanPostProcessor {

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName)
      throws BeansException {
    return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
  }
}
